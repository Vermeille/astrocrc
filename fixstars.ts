var FixedStars =
    {
      "Great Attractor": [
        {
          "name": "Great Attractor",
          "id": 0
        }
      ],
      "Virgo Cluster": [
        {
          "name": "Virgo Cluster",
          "id": 1
        },
        {
          "name": "Andromeda Galaxy",
          "id": 2
        }
      ],
      "Prasepe": [
        {
          "name": "Praesepe Cluster",
          "id": 3
        },
        {
          "name": "Polaris",
          "id": 4
        },
        {
          "name": "Deneb",
          "id": 5
        },
        {
          "name": "Deneb Adige",
          "id": 6
        },
        {
          "name": "Rigel",
          "id": 7
        },
        {
          "name": "Mira",
          "id": 8
        },
        {
          "name": "Ain",
          "id": 9
        }
      ],
      "Andromeda": [
        {
          "name": "Alpheratz",
          "id": 10
        },
        {
          "name": "Sirrah",
          "id": 11
        },
        {
          "name": "Mirach",
          "id": 12
        },
        {
          "name": "Almaak",
          "id": 13
        },
        {
          "name": "Almak",
          "id": 14
        },
        {
          "name": "Almac",
          "id": 15
        },
        {
          "name": "Almach",
          "id": 16
        },
        {
          "name": "Adhil",
          "id": 17
        },
        {
          "name": "Adhab",
          "id": 18
        },
        {
          "name": "Andromeda Galaxy",
          "id": 19
        }
      ],
      "Antila": [],
      "Apus": [],
      "Aquila": [
        {
          "name": "Altair",
          "id": 20
        },
        {
          "name": "Alshain",
          "id": 21
        },
        {
          "name": "Tarazed",
          "id": 22
        },
        {
          "name": "Al Mizan",
          "id": 23
        },
        {
          "name": "Deneb el Okab Borealis",
          "id": 24
        },
        {
          "name": "Deneb el Okab Australis",
          "id": 25
        },
        {
          "name": "Dheneb",
          "id": 26
        },
        {
          "name": "Bazak",
          "id": 27
        },
        {
          "name": "Tseen Foo",
          "id": 28
        },
        {
          "name": "Al Thalimaim Posterior",
          "id": 29
        },
        {
          "name": "Al Thalimaim Anterior",
          "id": 30
        },
        {
          "name": "Bered",
          "id": 31
        }
      ],
      "Aquarius": [
        {
          "name": "Sadalmelek",
          "id": 32
        },
        {
          "name": "Sadalmelik",
          "id": 33
        },
        {
          "name": "Sadalsuud",
          "id": 34
        },
        {
          "name": "Sadalachbia",
          "id": 35
        },
        {
          "name": "Skat",
          "id": 36
        },
        {
          "name": "Albali",
          "id": 37
        },
        {
          "name": "Altager",
          "id": 38
        },
        {
          "name": "Sadaltager",
          "id": 39
        },
        {
          "name": "Hydria",
          "id": 40
        },
        {
          "name": "Deli",
          "id": 41
        },
        {
          "name": "Ancha",
          "id": 42
        },
        {
          "name": "Situla",
          "id": 43
        },
        {
          "name": "Hydor",
          "id": 44
        },
        {
          "name": "Ekkhysis",
          "id": 45
        },
        {
          "name": "Albulaan",
          "id": 46
        },
        {
          "name": "Seat",
          "id": 47
        },
        {
          "name": "Bunda",
          "id": 48
        }
      ],
      "Ara": [
        {
          "name": "Ara",
          "id": 49
        }
      ],
      "Aries": [
        {
          "name": "Hamal",
          "id": 50
        },
        {
          "name": "Sheratan",
          "id": 51
        },
        {
          "name": "Mesarthim",
          "id": 52
        },
        {
          "name": "Botein",
          "id": 53
        }
      ],
      "Auriga": [
        {
          "name": "Capella",
          "id": 54
        },
        {
          "name": "Menkalinan",
          "id": 55
        },
        {
          "name": "Prijipati",
          "id": 56
        },
        {
          "name": "Maaz",
          "id": 57
        },
        {
          "name": "Al Anz",
          "id": 58
        },
        {
          "name": "Haedi",
          "id": 59
        },
        {
          "name": "Haedus",
          "id": 60
        },
        {
          "name": "Hoedus I",
          "id": 61
        },
        {
          "name": "Sadatoni",
          "id": 62
        },
        {
          "name": "Hoedus II",
          "id": 63
        },
        {
          "name": "Bogardus",
          "id": 64
        },
        {
          "name": "Manus",
          "id": 65
        },
        {
          "name": "Hasseleh",
          "id": 66
        },
        {
          "name": "Al Khabdhilinan",
          "id": 67
        }
      ],
      "Bootes": [
        {
          "name": "Arcturus",
          "id": 68
        },
        {
          "name": "Nekkar",
          "id": 69
        },
        {
          "name": "Seginus",
          "id": 70
        },
        {
          "name": "Haris",
          "id": 71
        },
        {
          "name": "Princeps",
          "id": 72
        },
        {
          "name": "Izar",
          "id": 73
        },
        {
          "name": "Mirak",
          "id": 74
        },
        {
          "name": "Pulcherrima",
          "id": 75
        },
        {
          "name": "Mufrid",
          "id": 76
        },
        {
          "name": "Muphrid",
          "id": 77
        },
        {
          "name": "Asellus Primus",
          "id": 78
        },
        {
          "name": "Asellus Secundus",
          "id": 79
        },
        {
          "name": "Asellus Tertius",
          "id": 80
        },
        {
          "name": "Alkalurops",
          "id": 81
        },
        {
          "name": "Hemelein Prima",
          "id": 82
        },
        {
          "name": "Al Hamalain",
          "id": 83
        },
        {
          "name": "Hemelein Secunda",
          "id": 84
        },
        {
          "name": "Ceginus",
          "id": 85
        },
        {
          "name": "Merga",
          "id": 86
        }
      ],
      "Caelum": [],
      "Camelopardalis": [],
      "Capricornus": [
        {
          "name": "Algedi",
          "id": 87
        },
        {
          "name": "Giedi Prima",
          "id": 88
        },
        {
          "name": "Algedi",
          "id": 89
        },
        {
          "name": "Giedi Secunda",
          "id": 90
        },
        {
          "name": "Dabih",
          "id": 91
        },
        {
          "name": "Nashira",
          "id": 92
        },
        {
          "name": "Deneb Algedi",
          "id": 93
        },
        {
          "name": "Castra",
          "id": 94
        },
        {
          "name": "Marakk",
          "id": 95
        },
        {
          "name": "Armus",
          "id": 96
        },
        {
          "name": "Dorsum",
          "id": 97
        },
        {
          "name": "Alshat",
          "id": 98
        },
        {
          "name": "Oculus",
          "id": 99
        },
        {
          "name": "Bos",
          "id": 100
        },
        {
          "name": "Pazan",
          "id": 101
        },
        {
          "name": "Pazhan",
          "id": 102
        },
        {
          "name": "Baten Algiedi",
          "id": 103
        }
      ],
      "Carina": [
        {
          "name": "Canopus",
          "id": 104
        },
        {
          "name": "Miaplacidus",
          "id": 105
        },
        {
          "name": "Avior",
          "id": 106
        },
        {
          "name": "Foramen",
          "id": 107
        },
        {
          "name": "Vathorz Posterior",
          "id": 108
        },
        {
          "name": "Scutulum",
          "id": 109
        },
        {
          "name": "Tureis",
          "id": 110
        },
        {
          "name": "Aspidiske",
          "id": 111
        },
        {
          "name": "Drus",
          "id": 112
        },
        {
          "name": "Drys",
          "id": 113
        },
        {
          "name": "Simiram",
          "id": 114
        },
        {
          "name": "Vathorz Prior",
          "id": 115
        }
      ],
      "Cassiopeia": [
        {
          "name": "Schedar",
          "id": 116
        },
        {
          "name": "Shedir",
          "id": 117
        },
        {
          "name": "Schedir",
          "id": 118
        },
        {
          "name": "Caph",
          "id": 119
        },
        {
          "name": "Tsih",
          "id": 120
        },
        {
          "name": "Cih",
          "id": 121
        },
        {
          "name": "Ruchbah",
          "id": 122
        },
        {
          "name": "Rucha",
          "id": 123
        },
        {
          "name": "Segin",
          "id": 124
        },
        {
          "name": "Achird",
          "id": 125
        },
        {
          "name": "Marfak",
          "id": 126
        }
      ],
      "Centaurus": [
        {
          "name": "Rigil Kent",
          "id": 127
        },
        {
          "name": "Rigel Kentaurus",
          "id": 128
        },
        {
          "name": "Toliman",
          "id": 129
        },
        {
          "name": "Bungula",
          "id": 130
        },
        {
          "name": "Hadar",
          "id": 131
        },
        {
          "name": "Agena",
          "id": 132
        },
        {
          "name": "Muhlifain",
          "id": 133
        },
        {
          "name": "Birdun",
          "id": 134
        },
        {
          "name": "Menkent",
          "id": 135
        },
        {
          "name": "Alhakim",
          "id": 136
        },
        {
          "name": "Ke Kwan",
          "id": 137
        },
        {
          "name": "Ma Ti",
          "id": 138
        },
        {
          "name": "Mati",
          "id": 139
        },
        {
          "name": "Kabkent Secunda",
          "id": 140
        },
        {
          "name": "Kabkent Tertia",
          "id": 141
        },
        {
          "name": "Proxima Centauri",
          "id": 142
        }
      ],
      "Cepheus": [
        {
          "name": "Alderamin",
          "id": 143
        },
        {
          "name": "Alphirk",
          "id": 144
        },
        {
          "name": "Alfirk",
          "id": 145
        },
        {
          "name": "Alrai",
          "id": 146
        },
        {
          "name": "Errai",
          "id": 147
        },
        {
          "name": "Alradif",
          "id": 148
        },
        {
          "name": "Alredif",
          "id": 149
        },
        {
          "name": "Phicares",
          "id": 150
        },
        {
          "name": "Phicareus",
          "id": 151
        },
        {
          "name": "Kurhah",
          "id": 152
        },
        {
          "name": "Alagemin",
          "id": 153
        },
        {
          "name": "Alkidr",
          "id": 154
        },
        {
          "name": "Alvahet",
          "id": 155
        },
        {
          "name": "Erakis",
          "id": 156
        },
        {
          "name": "The Garnet Star",
          "id": 157
        },
        {
          "name": "Kurdah",
          "id": 158
        },
        {
          "name": "Alkurhah",
          "id": 159
        },
        {
          "name": "Al Kalb al Rai",
          "id": 160
        }
      ],
      "Cetus": [
        {
          "name": "Menkar",
          "id": 161
        },
        {
          "name": "Diphda",
          "id": 162
        },
        {
          "name": "Difda",
          "id": 163
        },
        {
          "name": "Kaffaljidhma",
          "id": 164
        },
        {
          "name": "Phycochroma",
          "id": 165
        },
        {
          "name": "Baten Kaitos",
          "id": 166
        },
        {
          "name": "Deneb Algenubi",
          "id": 167
        },
        {
          "name": "Altawk",
          "id": 168
        },
        {
          "name": "Deneb Kaitos",
          "id": 169
        },
        {
          "name": "Shemali",
          "id": 170
        },
        {
          "name": "Menkar",
          "id": 171
        },
        {
          "name": "Mira",
          "id": 172
        },
        {
          "name": "Al Sadr al Ketus",
          "id": 173
        },
        {
          "name": "Abyssus Aqueus",
          "id": 174
        },
        {
          "name": "Al Nitham",
          "id": 175
        }
      ],
      "Chameleon": [],
      "Circinus": [],
      "Canis Major": [
        {
          "name": "Sirius",
          "id": 176
        },
        {
          "name": "Mirzam",
          "id": 177
        },
        {
          "name": "Murzim",
          "id": 178
        },
        {
          "name": "Muliphein",
          "id": 179
        },
        {
          "name": "Isis",
          "id": 180
        },
        {
          "name": "Wezen",
          "id": 181
        },
        {
          "name": "Adara",
          "id": 182
        },
        {
          "name": "Adhara",
          "id": 183
        },
        {
          "name": "Furud",
          "id": 184
        },
        {
          "name": "Aludra",
          "id": 185
        }
      ],
      "Canis Minor": [
        {
          "name": "Procyon",
          "id": 186
        },
        {
          "name": "Gomeisa",
          "id": 187
        }
      ],
      "Cancer": [
        {
          "name": "Acubens",
          "id": 188
        },
        {
          "name": "Al Tarf",
          "id": 189
        },
        {
          "name": "Asellus Borealis",
          "id": 190
        },
        {
          "name": "Asellus Australis",
          "id": 191
        },
        {
          "name": "Tegmen",
          "id": 192
        },
        {
          "name": "Tegmine",
          "id": 193
        },
        {
          "name": "Decapoda",
          "id": 194
        }
      ],
      "Columba": [
        {
          "name": "Phact",
          "id": 195
        },
        {
          "name": "Wazn",
          "id": 196
        },
        {
          "name": "Ghusn al Zaitun",
          "id": 197
        },
        {
          "name": "Al Kurud",
          "id": 198
        },
        {
          "name": "Tsze",
          "id": 199
        }
      ],
      "Coma Berenices": [
        {
          "name": "Diadem",
          "id": 200
        },
        {
          "name": "Aldafirah",
          "id": 201
        },
        {
          "name": "Kissin",
          "id": 202
        }
      ],
      "Corona Borealis": [
        {
          "name": "Alphecca",
          "id": 203
        },
        {
          "name": "Alphekka",
          "id": 204
        },
        {
          "name": "Gemma",
          "id": 205
        },
        {
          "name": "Nusakan",
          "id": 206
        },
        {
          "name": "The Blaze Star",
          "id": 207
        }
      ],
      "Corona Australis": [
        {
          "name": "Alfecca Meridiana",
          "id": 208
        }
      ],
      "Crater": [
        {
          "name": "Alkes",
          "id": 209
        },
        {
          "name": "Alsharasif",
          "id": 210
        },
        {
          "name": "Labrum",
          "id": 211
        }
      ],
      "Crux": [],
      "Acrux": [
        {
          "name": "Acrux",
          "id": 212
        },
        {
          "name": "Mimosa",
          "id": 213
        },
        {
          "name": "Gacrux",
          "id": 214
        },
        {
          "name": "Decrux",
          "id": 215
        },
        {
          "name": "Juxta Crucem",
          "id": 216
        }
      ],
      "Corvus": [
        {
          "name": "Alchiba",
          "id": 217
        },
        {
          "name": "Alchita",
          "id": 218
        },
        {
          "name": "Kraz",
          "id": 219
        },
        {
          "name": "Gienah Corvi",
          "id": 220
        },
        {
          "name": "Algorab",
          "id": 221
        },
        {
          "name": "Minkar",
          "id": 222
        },
        {
          "name": "Avis Satyra",
          "id": 223
        }
      ],
      "Canes Venatici": [
        {
          "name": "Cor Caroli",
          "id": 224
        },
        {
          "name": "Asterion",
          "id": 225
        },
        {
          "name": "Chara",
          "id": 226
        }
      ],
      "Cygnus": [
        {
          "name": "Deneb",
          "id": 227
        },
        {
          "name": "Albireo",
          "id": 228
        },
        {
          "name": "Sador",
          "id": 229
        },
        {
          "name": "Sadir",
          "id": 230
        },
        {
          "name": "Sadr",
          "id": 231
        },
        {
          "name": "Ruc",
          "id": 232
        },
        {
          "name": "Rukh",
          "id": 233
        },
        {
          "name": "Urakhga",
          "id": 234
        },
        {
          "name": "Al Fawaris",
          "id": 235
        },
        {
          "name": "Gienah Cygni",
          "id": 236
        },
        {
          "name": "Gienah Ghurab",
          "id": 237
        },
        {
          "name": "Azelfafage",
          "id": 238
        },
        {
          "name": "Ruchbah I",
          "id": 239
        },
        {
          "name": "Ruchbah II",
          "id": 240
        }
      ],
      "Delphinus": [
        {
          "name": "Sualocin",
          "id": 241
        },
        {
          "name": "Rotanev",
          "id": 242
        },
        {
          "name": "Deneb Dulphim",
          "id": 243
        }
      ],
      "Dorado": [],
      "Draco": [
        {
          "name": "Thuban",
          "id": 244
        },
        {
          "name": "Alwaid",
          "id": 245
        },
        {
          "name": "Rastaban",
          "id": 246
        },
        {
          "name": "Eltanin",
          "id": 247
        },
        {
          "name": "Etamin",
          "id": 248
        },
        {
          "name": "Nodus II",
          "id": 249
        },
        {
          "name": "Altais",
          "id": 250
        },
        {
          "name": "Tyl",
          "id": 251
        },
        {
          "name": "Nodus I",
          "id": 252
        },
        {
          "name": "Aldhibah",
          "id": 253
        },
        {
          "name": "Alsafi",
          "id": 254
        },
        {
          "name": "Edasich",
          "id": 255
        },
        {
          "name": "Ed Asich",
          "id": 256
        },
        {
          "name": "Ketu",
          "id": 257
        },
        {
          "name": "Giansar",
          "id": 258
        },
        {
          "name": "Gianfar",
          "id": 259
        },
        {
          "name": "Arrakis",
          "id": 260
        },
        {
          "name": "Kuma",
          "id": 261
        },
        {
          "name": "Kuma",
          "id": 262
        },
        {
          "name": "Grumium",
          "id": 263
        },
        {
          "name": "Alsafi",
          "id": 264
        },
        {
          "name": "Athafi",
          "id": 265
        },
        {
          "name": "Batentaban Borealis",
          "id": 266
        },
        {
          "name": "Dziban",
          "id": 267
        },
        {
          "name": "Alathfar",
          "id": 268
        },
        {
          "name": "Al Athfar",
          "id": 269
        },
        {
          "name": "Aldhibain",
          "id": 270
        },
        {
          "name": "Batentaban Australis",
          "id": 271
        }
      ],
      "Equuleus": [
        {
          "name": "Kitalpha",
          "id": 272
        }
      ],
      "Eridanus": [
        {
          "name": "Achernar",
          "id": 273
        },
        {
          "name": "Cursa",
          "id": 274
        },
        {
          "name": "Zaurak",
          "id": 275
        },
        {
          "name": "Rana",
          "id": 276
        },
        {
          "name": "Azha",
          "id": 277
        },
        {
          "name": "Acamar",
          "id": 278
        },
        {
          "name": "Zibal",
          "id": 279
        },
        {
          "name": "Beid",
          "id": 280
        },
        {
          "name": "Keid",
          "id": 281
        },
        {
          "name": "Angetenar",
          "id": 282
        },
        {
          "name": "Theemin",
          "id": 283
        },
        {
          "name": "Sceptrum",
          "id": 284
        }
      ],
      "Fornax": [
        {
          "name": "Fornacis",
          "id": 285
        }
      ],
      "Gemini": [
        {
          "name": "Castor",
          "id": 286
        },
        {
          "name": "Pollux",
          "id": 287
        },
        {
          "name": "Alhena",
          "id": 288
        },
        {
          "name": "Almeisan",
          "id": 289
        },
        {
          "name": "Wasat",
          "id": 290
        },
        {
          "name": "Mebsuta",
          "id": 291
        },
        {
          "name": "Mekbuda",
          "id": 292
        },
        {
          "name": "Propus etaGem",
          "id": 293
        },
        {
          "name": "Nageba",
          "id": 294
        },
        {
          "name": "Propus iotGem",
          "id": 295
        },
        {
          "name": "Al Krikab",
          "id": 296
        },
        {
          "name": "Kebash",
          "id": 297
        },
        {
          "name": "Alkibash",
          "id": 298
        },
        {
          "name": "Tejat",
          "id": 299
        },
        {
          "name": "Alzirr",
          "id": 300
        }
      ],
      "Grus": [
        {
          "name": "Alnair",
          "id": 301
        },
        {
          "name": "Gruid",
          "id": 302
        },
        {
          "name": "Al Dhanab",
          "id": 303
        },
        {
          "name": "Ras Alkurki",
          "id": 304
        }
      ],
      "Hercules": [
        {
          "name": "Ras Algethi",
          "id": 305
        },
        {
          "name": "Rasalgethi",
          "id": 306
        },
        {
          "name": "Kornephoros",
          "id": 307
        },
        {
          "name": "Rutilicus",
          "id": 308
        },
        {
          "name": "Sarin",
          "id": 309
        },
        {
          "name": "Kajam epsHer",
          "id": 310
        },
        {
          "name": "Sofian",
          "id": 311
        },
        {
          "name": "Rukbalgethi Genubi",
          "id": 312
        },
        {
          "name": "Al Jathiyah",
          "id": 313
        },
        {
          "name": "Marsik",
          "id": 314
        },
        {
          "name": "Marfik",
          "id": 315
        },
        {
          "name": "Masym",
          "id": 316
        },
        {
          "name": "Maasym",
          "id": 317
        },
        {
          "name": "Melkarth",
          "id": 318
        },
        {
          "name": "Fudail",
          "id": 319
        },
        {
          "name": "Rukbalgethi Shemali",
          "id": 320
        },
        {
          "name": "Kajam omeHer",
          "id": 321
        },
        {
          "name": "Cujam",
          "id": 322
        },
        {
          "name": "Apex",
          "id": 323
        }
      ],
      "Horologium": [],
      "Hydra": [
        {
          "name": "Alphard",
          "id": 324
        },
        {
          "name": "Cor Hydrae",
          "id": 325
        },
        {
          "name": "Cauda Hydrae",
          "id": 326
        },
        {
          "name": "Dhanab al Shuja",
          "id": 327
        },
        {
          "name": "Mautinah",
          "id": 328
        },
        {
          "name": "Ashlesha",
          "id": 329
        },
        {
          "name": "Hydrobius",
          "id": 330
        },
        {
          "name": "Pleura",
          "id": 331
        },
        {
          "name": "Sataghni",
          "id": 332
        },
        {
          "name": "Al Minliar al Shuja",
          "id": 333
        },
        {
          "name": "Minchir",
          "id": 334
        },
        {
          "name": "Ukdah",
          "id": 335
        },
        {
          "name": "Ukdah",
          "id": 336
        }
      ],
      "Hydrus": [],
      "Indus": [],
      "Lacerta": [],
      "Leo": [
        {
          "name": "Regulus",
          "id": 337
        },
        {
          "name": "Denebola",
          "id": 338
        },
        {
          "name": "Algieba",
          "id": 339
        },
        {
          "name": "Dhur",
          "id": 340
        },
        {
          "name": "Zosma",
          "id": 341
        },
        {
          "name": "Ras Elased Australis",
          "id": 342
        },
        {
          "name": "Adhafera",
          "id": 343
        },
        {
          "name": "Algieba",
          "id": 344
        },
        {
          "name": "Al Jabhah",
          "id": 345
        },
        {
          "name": "Tse Tseng",
          "id": 346
        },
        {
          "name": "Tsze Tseang",
          "id": 347
        },
        {
          "name": "Alminhar",
          "id": 348
        },
        {
          "name": "Al Minliar al Asad",
          "id": 349
        },
        {
          "name": "Alterf",
          "id": 350
        },
        {
          "name": "Ras Elased Borealis",
          "id": 351
        },
        {
          "name": "Rasalas",
          "id": 352
        },
        {
          "name": "Subra",
          "id": 353
        },
        {
          "name": "Shishimai",
          "id": 354
        },
        {
          "name": "Coxa",
          "id": 355
        },
        {
          "name": "Chertan",
          "id": 356
        },
        {
          "name": "Cestan",
          "id": 357
        },
        {
          "name": "Chort",
          "id": 358
        },
        {
          "name": "Shir",
          "id": 359
        }
      ],
      "Lepus": [
        {
          "name": "Arneb",
          "id": 360
        },
        {
          "name": "Nihal",
          "id": 361
        },
        {
          "name": "Sasin",
          "id": 362
        }
      ],
      "Libra": [
        {
          "name": "Zubenelgenubi",
          "id": 363
        },
        {
          "name": "Zuben Elgenubi",
          "id": 364
        },
        {
          "name": "Zubeneshamali",
          "id": 365
        },
        {
          "name": "Zuben Eshamali",
          "id": 366
        },
        {
          "name": "Zubenelakrab",
          "id": 367
        },
        {
          "name": "Zuben Elakrab",
          "id": 368
        },
        {
          "name": "Zubenelakribi",
          "id": 369
        },
        {
          "name": "Zuben Elakribi",
          "id": 370
        },
        {
          "name": "Zubenhakrabi",
          "id": 371
        },
        {
          "name": "Zuben Hakrabi",
          "id": 372
        },
        {
          "name": "Brachium",
          "id": 373
        }
      ],
      "Leo Minor": [
        {
          "name": "Praecipua",
          "id": 374
        }
      ],
      "Lupus": [
        {
          "name": "Kakkab",
          "id": 375
        },
        {
          "name": "Men",
          "id": 376
        },
        {
          "name": "Kekouan",
          "id": 377
        },
        {
          "name": "Thusia",
          "id": 378
        },
        {
          "name": "Hilasmus",
          "id": 379
        }
      ],
      "Lynx": [
        {
          "name": "Alvashak",
          "id": 380
        },
        {
          "name": "Al Fahd",
          "id": 381
        },
        {
          "name": "Alsciaukat",
          "id": 382
        },
        {
          "name": "Mabsuthat",
          "id": 383
        },
        {
          "name": "Mabsuthat",
          "id": 384
        },
        {
          "name": "Maculosa",
          "id": 385
        },
        {
          "name": "Maculata",
          "id": 386
        }
      ],
      "Lyra": [
        {
          "name": "Vega",
          "id": 387
        },
        {
          "name": "Sheliak",
          "id": 388
        },
        {
          "name": "Sulaphat",
          "id": 389
        },
        {
          "name": "Sulafat",
          "id": 390
        },
        {
          "name": "Aladfar",
          "id": 391
        },
        {
          "name": "Alathfar",
          "id": 392
        }
      ],
      "Mensa": [],
      "Microscopium": [],
      "Monoceros": [],
      "Musca": [],
      "Norma": [],
      "Octans": [
        {
          "name": "Polaris Australis",
          "id": 393
        }
      ],
      "Ophiuchus": [
        {
          "name": "Rasalhague",
          "id": 394
        },
        {
          "name": "Celbalrai",
          "id": 395
        },
        {
          "name": "Kelb Alrai",
          "id": 396
        },
        {
          "name": "Al Durajah",
          "id": 397
        },
        {
          "name": "Yed Prior",
          "id": 398
        },
        {
          "name": "Yed Posterior",
          "id": 399
        },
        {
          "name": "Han",
          "id": 400
        },
        {
          "name": "Sabik",
          "id": 401
        },
        {
          "name": "Imad",
          "id": 402
        },
        {
          "name": "Helkath",
          "id": 403
        },
        {
          "name": "Marfik",
          "id": 404
        },
        {
          "name": "Sinistra",
          "id": 405
        },
        {
          "name": "Barnard's star",
          "id": 406
        }
      ],
      "Orion": [
        {
          "name": "Betelgeuse",
          "id": 407
        },
        {
          "name": "Beteigeuse",
          "id": 408
        },
        {
          "name": "Rigel",
          "id": 409
        },
        {
          "name": "Bellatrix",
          "id": 410
        },
        {
          "name": "Mintaka",
          "id": 411
        },
        {
          "name": "Alnilam",
          "id": 412
        },
        {
          "name": "Alnitak",
          "id": 413
        },
        {
          "name": "Trapezium",
          "id": 414
        },
        {
          "name": "Hatsya",
          "id": 415
        },
        {
          "name": "Nair al Saif",
          "id": 416
        },
        {
          "name": "Saiph",
          "id": 417
        },
        {
          "name": "Heka",
          "id": 418
        },
        {
          "name": "Meissa",
          "id": 419
        },
        {
          "name": "Tabit",
          "id": 420
        },
        {
          "name": "Tabit",
          "id": 421
        },
        {
          "name": "Thabit",
          "id": 422
        }
      ],
      "Pavo": [
        {
          "name": "Peacock",
          "id": 423
        }
      ],
      "Phoenix": [
        {
          "name": "Ankaa",
          "id": 424
        }
      ],
      "Pegasus": [
        {
          "name": "Markab",
          "id": 425
        },
        {
          "name": "Scheat",
          "id": 426
        },
        {
          "name": "Algenib",
          "id": 427
        },
        {
          "name": "Enif",
          "id": 428
        },
        {
          "name": "Homam",
          "id": 429
        },
        {
          "name": "Matar",
          "id": 430
        },
        {
          "name": "Biham",
          "id": 431
        },
        {
          "name": "Baham",
          "id": 432
        },
        {
          "name": "Jih",
          "id": 433
        },
        {
          "name": "Sadalbari",
          "id": 434
        },
        {
          "name": "Kerb",
          "id": 435
        },
        {
          "name": "Salm",
          "id": 436
        }
      ],
      "Perseus": [
        {
          "name": "Mirfak",
          "id": 437
        },
        {
          "name": "Mirphak",
          "id": 438
        },
        {
          "name": "Algol",
          "id": 439
        },
        {
          "name": "Atik",
          "id": 440
        },
        {
          "name": "Miram",
          "id": 441
        },
        {
          "name": "Misam",
          "id": 442
        },
        {
          "name": "Menkib",
          "id": 443
        },
        {
          "name": "Atiks",
          "id": 444
        },
        {
          "name": "Gorgona Secunda",
          "id": 445
        },
        {
          "name": "Gorgona Tertia",
          "id": 446
        },
        {
          "name": "Gorgona Quatra",
          "id": 447
        },
        {
          "name": "Capulus",
          "id": 448
        }
      ],
      "Pictor": [],
      "Piscis Austrinus": [
        {
          "name": "Fomalhaut",
          "id": 449
        },
        {
          "name": "Tien Kang",
          "id": 450
        },
        {
          "name": "Aboras",
          "id": 451
        }
      ],
      "Pisces": [
        {
          "name": "Alrischa",
          "id": 452
        },
        {
          "name": "Fum Alsamakah",
          "id": 453
        },
        {
          "name": "Samakah",
          "id": 454
        },
        {
          "name": "Simmah",
          "id": 455
        },
        {
          "name": "Linteum",
          "id": 456
        },
        {
          "name": "Kaht",
          "id": 457
        },
        {
          "name": "Al Pherg",
          "id": 458
        },
        {
          "name": "Torcularis Septentrionalis",
          "id": 459
        },
        {
          "name": "Anunitum",
          "id": 460
        },
        {
          "name": "Vernalis",
          "id": 461
        }
      ],
      "Puppis": [
        {
          "name": "Naos",
          "id": 462
        },
        {
          "name": "Suhail Hadar",
          "id": 463
        },
        {
          "name": "Kaimana",
          "id": 464
        },
        {
          "name": "Azmidiske",
          "id": 465
        },
        {
          "name": "Ahadi",
          "id": 466
        },
        {
          "name": "Turais",
          "id": 467
        },
        {
          "name": "Al Rihla",
          "id": 468
        },
        {
          "name": "Rehla",
          "id": 469
        },
        {
          "name": "Anazitisi",
          "id": 470
        }
      ],
      "Pyxis": [],
      "Reticulum": [],
      "Sculptor": [],
      "Scorpius": [
        {
          "name": "Antares",
          "id": 471
        },
        {
          "name": "Graffias",
          "id": 472
        },
        {
          "name": "Akrab",
          "id": 473
        },
        {
          "name": "Acrab",
          "id": 474
        }
      ],
      "Aculeus": [
        {
          "name": "Aculeus",
          "id": 475
        }
      ],
      "Acumen": [
        {
          "name": "Acumen",
          "id": 476
        },
        {
          "name": "Dschubba",
          "id": 477
        },
        {
          "name": "Wei",
          "id": 478
        },
        {
          "name": "Sargas",
          "id": 479
        },
        {
          "name": "Girtab",
          "id": 480
        },
        {
          "name": "Shaula",
          "id": 481
        },
        {
          "name": "Jabbah",
          "id": 482
        },
        {
          "name": "Grafias",
          "id": 483
        },
        {
          "name": "Alniyat",
          "id": 484
        },
        {
          "name": "Lesath",
          "id": 485
        },
        {
          "name": "Jabhat al Akrab",
          "id": 486
        },
        {
          "name": "Jabhat al Akrab",
          "id": 487
        }
      ],
      "Scutum": [],
      "Serpens": [
        {
          "name": "Unukalhai",
          "id": 488
        },
        {
          "name": "Cor Serpentis",
          "id": 489
        },
        {
          "name": "Chow",
          "id": 490
        },
        {
          "name": "Zhou",
          "id": 491
        },
        {
          "name": "Ainalhai",
          "id": 492
        },
        {
          "name": "Qin",
          "id": 493
        },
        {
          "name": "Chin",
          "id": 494
        },
        {
          "name": "Nulla Pambu",
          "id": 495
        },
        {
          "name": "Tang",
          "id": 496
        },
        {
          "name": "Alya",
          "id": 497
        },
        {
          "name": "Leiolepis",
          "id": 498
        },
        {
          "name": "Leiolepidotus",
          "id": 499
        },
        {
          "name": "Nehushtan",
          "id": 500
        }
      ],
      "Sextans": [],
      "Sagitta": [
        {
          "name": "Sham",
          "id": 501
        }
      ],
      "Sagittarius": [
        {
          "name": "Rukbat",
          "id": 502
        },
        {
          "name": "Arkab Prior",
          "id": 503
        },
        {
          "name": "Arkab Posterior",
          "id": 504
        },
        {
          "name": "Alnasl",
          "id": 505
        },
        {
          "name": "Nash",
          "id": 506
        },
        {
          "name": "Kaus Medis",
          "id": 507
        },
        {
          "name": "Kaus Meridionalis",
          "id": 508
        },
        {
          "name": "Kaus Australis",
          "id": 509
        },
        {
          "name": "Ascella",
          "id": 510
        },
        {
          "name": "Sephdar",
          "id": 511
        },
        {
          "name": "Ira Furoris",
          "id": 512
        },
        {
          "name": "Kaus Borealis",
          "id": 513
        },
        {
          "name": "Polis",
          "id": 514
        },
        {
          "name": "Ain al Rami",
          "id": 515
        },
        {
          "name": "Manubrium",
          "id": 516
        },
        {
          "name": "Albaldah",
          "id": 517
        },
        {
          "name": "Nunki",
          "id": 518
        },
        {
          "name": "Hecatebolus",
          "id": 519
        },
        {
          "name": "Nanto",
          "id": 520
        },
        {
          "name": "Terebellium",
          "id": 521
        },
        {
          "name": "Gal. Center",
          "id": 522
        },
        {
          "name": "Facies",
          "id": 523
        },
        {
          "name": "Spiculum",
          "id": 524
        }
      ],
      "Taurus": [
        {
          "name": "Aldebaran",
          "id": 525
        },
        {
          "name": "Elnath",
          "id": 526
        },
        {
          "name": "El Nath",
          "id": 527
        },
        {
          "name": "Alnath",
          "id": 528
        },
        {
          "name": "Prima Hyadum",
          "id": 529
        },
        {
          "name": "Hyadum I",
          "id": 530
        },
        {
          "name": "Secunda Hyadum",
          "id": 531
        },
        {
          "name": "Hyadum II",
          "id": 532
        },
        {
          "name": "Ain",
          "id": 533
        },
        {
          "name": "Al Hecka",
          "id": 534
        },
        {
          "name": "Alcyone",
          "id": 535
        },
        {
          "name": "Phaeo",
          "id": 536
        },
        {
          "name": "Phaesula",
          "id": 537
        },
        {
          "name": "Althaur",
          "id": 538
        },
        {
          "name": "Kattupothu",
          "id": 539
        },
        {
          "name": "Furibundus",
          "id": 540
        },
        {
          "name": "Ushakaron",
          "id": 541
        },
        {
          "name": "Atirsagne",
          "id": 542
        },
        {
          "name": "Celeano",
          "id": 543
        },
        {
          "name": "Electra",
          "id": 544
        },
        {
          "name": "Taygeta",
          "id": 545
        },
        {
          "name": "Maia",
          "id": 546
        },
        {
          "name": "Asterope",
          "id": 547
        },
        {
          "name": "Sterope I",
          "id": 548
        },
        {
          "name": "Sterope II",
          "id": 549
        },
        {
          "name": "Merope",
          "id": 550
        },
        {
          "name": "Atlas",
          "id": 551
        },
        {
          "name": "Pleione",
          "id": 552
        }
      ],
      "Telescopium": [],
      "Triangulum Australe": [
        {
          "name": "Atria",
          "id": 553
        }
      ],
      "Triangulum": [
        {
          "name": "Ras Mutallah",
          "id": 554
        },
        {
          "name": "Metallah",
          "id": 555
        }
      ],
      "Tucana": [],
      "Ursa Major": [
        {
          "name": "Dubhe",
          "id": 556
        },
        {
          "name": "Merak",
          "id": 557
        },
        {
          "name": "Phecda",
          "id": 558
        },
        {
          "name": "Megrez",
          "id": 559
        },
        {
          "name": "Alioth",
          "id": 560
        },
        {
          "name": "Mizar",
          "id": 561
        },
        {
          "name": "Alkaid",
          "id": 562
        },
        {
          "name": "Benetnash",
          "id": 563
        },
        {
          "name": "Al Haud",
          "id": 564
        },
        {
          "name": "Talitha Borealis",
          "id": 565
        },
        {
          "name": "Talitha Australis",
          "id": 566
        },
        {
          "name": "Tania Borealis",
          "id": 567
        },
        {
          "name": "Tania Australis",
          "id": 568
        },
        {
          "name": "Alula Borealis",
          "id": 569
        },
        {
          "name": "Alula Australis",
          "id": 570
        },
        {
          "name": "Muscida",
          "id": 571
        },
        {
          "name": "El Kophrah",
          "id": 572
        },
        {
          "name": "Alcor",
          "id": 573
        },
        {
          "name": "Saidak",
          "id": 574
        }
      ],
      "Ursa Minor": [
        {
          "name": "Polaris",
          "id": 575
        },
        {
          "name": "Kochab",
          "id": 576
        },
        {
          "name": "Pherkad",
          "id": 577
        },
        {
          "name": "Yildun",
          "id": 578
        },
        {
          "name": "Urodelus",
          "id": 579
        },
        {
          "name": "Alifa Al Farkadain",
          "id": 580
        },
        {
          "name": "Farkadain",
          "id": 581
        },
        {
          "name": "Pharkadain",
          "id": 582
        },
        {
          "name": "Anwar al Farkadain",
          "id": 583
        },
        {
          "name": "Pherkad Minor",
          "id": 584
        }
      ],
      "Vela": [
        {
          "name": "Suhail al Muhlif",
          "id": 585
        },
        {
          "name": "Regor",
          "id": 586
        },
        {
          "name": "Koo She",
          "id": 587
        },
        {
          "name": "Markeb",
          "id": 588
        },
        {
          "name": "Alsuhail",
          "id": 589
        },
        {
          "name": "Suhail",
          "id": 590
        },
        {
          "name": "Peregrini",
          "id": 591
        },
        {
          "name": "Alherem",
          "id": 592
        },
        {
          "name": "Xestus",
          "id": 593
        },
        {
          "name": "Tseen Ke",
          "id": 594
        }
      ],
      "Virgo": [
        {
          "name": "Spica",
          "id": 595
        },
        {
          "name": "Zavijava",
          "id": 596
        },
        {
          "name": "Alaraph",
          "id": 597
        },
        {
          "name": "Porrima",
          "id": 598
        },
        {
          "name": "Auva",
          "id": 599
        },
        {
          "name": "Vindemiatrix",
          "id": 600
        },
        {
          "name": "Heze",
          "id": 601
        },
        {
          "name": "Zaniah",
          "id": 602
        },
        {
          "name": "Syrma",
          "id": 603
        },
        {
          "name": "Khambalia",
          "id": 604
        },
        {
          "name": "Rijl al Awwa",
          "id": 605
        },
        {
          "name": "Ril Alauva",
          "id": 606
        }
      ],
      "Volans": [],
      "Vulpecula": [
        {
          "name": "Anser",
          "id": 607
        }
      ]
    };

