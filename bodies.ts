class Body {
    speed: number;
    name: string;
    pos: number;
    sign: number;
    house: number;
    fetch: (Sky) => number;

    constructor(name_, pos_) {
        this.name = name_;
        this.pos = pos_;
    }
}

class Sky {
    planets: Body[];
    stars: Body[];
    lots: Body[];
    cusps: Body[];
    Asc: Body;
    MC: Body;

    constructor() {
        this.planets = [];
        this.stars = [];
        this.lots = [];
        this.cusps = [];
    }

    MkBody(name: string, pos: number): Body {
        var body = new Body(name, pos);
        this.RefreshBody(body, pos);
        return body;
    }
    RefreshBody(b: Body, pos: number): void {
        b.pos = pos;
        b.sign = Math.floor(b.pos / 30);
        b.house = (b.sign - this.Asc.sign + 12) % 12 + 1;
    }

    AddPlanet(p: Planets, pos: number, speed: number): void {
        this.planets[p] = this.MkBody(Planets[p], pos);
        this.planets[p].speed = speed;
        if (speed < 0) {
            $("#" + Planets[p]).addClass("cls-retro");
        } else {
            $("#" + Planets[p]).removeClass("cls-retro");
        }
    }

    AddStar(star: any, pos: number): void {
        this.stars[star.id] = this.MkBody(star.name, pos);
        $("#stars-chart").append("<div id='" + star.id + "'>" + star.name.substr(0, 3) + "</div>");
        this.RefreshStar(star.id);
    }

    AddLot(lot: any): void {
        this.lots[lot.id] = this.MkBody(lot.name, lot.fetch(this));
        this.lots[lot.id].fetch = lot.fetch;
        $("#lots-chart").append("<div id='" + lot.id + "'>" + lot.name.substr(0, 3) + "</div>");
        this.RefreshLot(this.lots[lot.id]);
    }

    AddOther(name: string, pos: number): void {
        if (name == "Asc") {
            this.Asc = new Body(name, pos);
            this.Asc.house = 1;
            this.Asc.sign = Math.floor(pos / 30);
        } else {
            this[name] = this.MkBody(name, pos);
        }
    }

    RefreshStar(star_id: number): void {
        var that = this;
        $.getJSON(
            "fixstar?" + $("#coords").serialize() + "&name=" + encodeURIComponent(this.stars[star_id].name),
            function (data) {
                that.RefreshBody(that.stars[star_id], data.pos);
                RefreshUI(that);
            }
        );
    }

    RefreshLot(lot: Body): void {
        this.RefreshBody(lot, lot.fetch(this));
        RefreshUI(this);
    }

    RefreshLots(): void {
        for (var l in this.lots) {
            this.RefreshLot(this.lots[l]);
        }
    }

    Render(): void {
        PlaceSigns(this.Asc.pos);
        ForeachBody(this, (s, b, id) => {
            PlaceOneSymbol(s.Asc, b, id);
            var angle = b.pos % 30;
            angle = angle /30.0;
            var angle_str = (500 * angle) + "px";
            if ($("#" + id + "-hor").length != 0) {
                $("#" + id + "-hor").animate({left: angle_str});
            } else {
                $("#horizontal").append(
                    '<div id="' + id + '-hor" style="left: ' + angle_str + '; top: 20px;">'
                    + b.name.substr(0, 3)
                    + '</div>');
            }
        });
    }
}

function ForeachPermanent(s: Sky, f): void {
    for (var p in s.planets)
        f(s, s.planets[p], Planets[p]);

    for (var p in s.cusps)
        f(s, s.cusps[p], p);

    f(s, s.Asc, "Asc");
    f(s, s.MC, "MC");
    f(s, s["Caput_Draconis"], "Caput_Draconis");
    f(s, s["Cauda_Draconis"], "Cauda_Draconis");
}

function ForeachBody(s: Sky, f): void {
    for (var p in s.planets)
        f(s, s.planets[p], Planets[p]);

    for (var p in s.stars)
        f(s, s.stars[p], p);

    for (var p in s.lots)
        f(s, s.lots[p], p);

    for (var p in s.cusps)
        f(s, s.cusps[p], p);

    f(s, s.Asc, "Asc");
    f(s, s.MC, "MC");
    f(s, s["Caput_Draconis"], "Caput_Draconis");
    f(s, s["Cauda_Draconis"], "Cauda_Draconis");
}
