FROM ubuntu:15.10

RUN apt-get update && apt-get install -y haskell-platform make npm \
        libsqlite3-dev && \
        npm install -g typescript@1.8.10 && \
        ln -s "$(which nodejs)" /usr/bin/node && \
        cabal update

WORKDIR /root

ADD . /root

RUN make

EXPOSE 8000

WORKDIR /root/build

CMD ./astro
