#ifndef MATH_H
#define MATH_H

#include <cmath>

namespace Math
{
    class Degree
    {
    public:
        explicit Degree(double d)
            : _val(d)
        {
        }
        double val() { return _val; }
        void set(double d) { _val = d; }
        operator double() { return _val; }
    private:
        double _val;
    };

    class Radian
    {
    public:
        explicit Radian(double d)
            : _val(d)
        {
        }
        double val() { return _val; }
        void set(double d) { _val = d; }
        operator double() { return _val; }
    private:
        double _val;
    };

    double Cos(Degree d);
    double Cos(double d);

    double Sin(Degree d);
    double Sin(double d);

    double Asin(Degree d);
    double Asin(double d);

    double Acos(Degree d);
    double Acos(double d);
}

#endif // MATH_H
