#include "math.h"


namespace Math
{
    double Cos(Degree d) { return cos(M_PI/180 * d.val()); }
    double Cos(double d) { return cos(d); }

    double Sin(Degree d) { return sin(M_PI/180 * d.val()); }
    double Sin(double d) { return sin(d); }

    double Asin(Degree d) { return asin(M_PI/180 * d.val()); }
    double Asin(double d) { return asin(d); }

    double Acos(Degree d) { return acos(M_PI/180 * d.val()); }
    double Acos(double d) { return acos(d); }
}
