///<reference path="jquery.d.ts"/>
///<reference path="bodies.ts"/>
///<reference path="ui.ts"/>
///<reference path="fixstars.ts"/>

interface JQuery
{
    typeahead(arg: any): any;
}

interface JQueryStatic
{
    Notify(arg: any): void;
}

var $inputs;

function SetPanels()
{
    $("#placeholder-change .placeholder").hide().first().show();

    $("#menu a").each(function() {
        $(this).click(function() {
            $("#placeholder-change .placeholder").hide();
            $($(this).attr('href')).show();
        });
    });

}

function RefreshChart(sky: Sky, json) {
    if (json.sky) {
        sky.AddOther("Asc", json.sky.Asc);
        sky.AddOther("MC", json.sky.MC);
        sky.AddOther("Caput_Draconis", json.sky["TrueNode"].pos);
        sky.AddOther("Cauda_Draconis", (json.sky["TrueNode"].pos + 180) % 360);
        for (var b of PlntIterator) {
            var plnt = json.sky[Planets[b]];
            sky.AddPlanet(b, plnt.pos, plnt.speed);
        }
        sky.RefreshLots();
    }
    RefreshUI(sky);
}

function SetDate(date) {
    $inputs.day.val(date.getDate());
    $inputs.month.val(date.getMonth() + 1);
    $inputs.year.val(date.getFullYear());

    $inputs.hour.val(date.getHours());
    $inputs.min.val(date.getMinutes());
    $inputs.sec.val(date.getSeconds());
}

function GetDate() {
    var d = new Date();
    d.setFullYear($inputs.year.val());
    d.setMonth($inputs.month.val() - 1);
    d.setDate($inputs.day.val());
    d.setHours($inputs.hour.val());
    d.setMinutes($inputs.min.val());
    d.setSeconds($inputs.sec.val());
    return d;
}

function SetIncDateButton()
{
    $("#inc").click(function() {
        var d = GetDate();
        var unit = $("#inc-unit").val();

        if (unit == "year") {
            d.setFullYear(d.getFullYear() + 1);
        } else if (unit == "month") {
            d.setMonth(d.getMonth() + 1);
        } else if (unit == "day") {
            d.setDate(d.getDate() + 1);
        } else if (unit == "hour") {
            d.setHours(d.getHours() + 1);
        } else if (unit == "min") {
            d.setMinutes(d.getMinutes() + 1);
        } else {
            d.setSeconds(d.getSeconds() + 1)
        }
        SetDate(d);
        $("#catch").click();
    });
}

function SetDecDateButton()
{
    $("#dec").click(function() {
        var d = GetDate();
        var unit = $("#inc-unit").val();

        if (unit == "year") {
            d.setFullYear(d.getFullYear() - 1);
        } else if (unit == "month") {
            d.setMonth(d.getMonth() - 1);
        } else if (unit == "day") {
            d.setDate(d.getDate() - 1);
        } else if (unit == "hour") {
            d.setHours(d.getHours() - 1);
        } else if (unit == "min") {
            d.setMinutes(d.getMinutes() - 1);
        } else {
            d.setSeconds(d.getSeconds() - 1)
        }
        SetDate(d);
        $("#catch").click();
    });
}

function rt() {
    var now = new Date();

    SetDate(now);

    if ($("#catch").click())
        if ($("#realtime").prop('checked'))
            window.setTimeout(rt, 1000);
}

function Trutine(data) {
    var $trutine = $("#trutine");
    $trutine.empty();
    function DateToURL(d) {
        return "/?year=" + d.getFullYear() + "&month=" + (d.getMonth() + 1)
            + "&day=" + d.getDate() + "&hour=" + d.getHours() + "&min="
            + d.getMinutes() + "&sec=" + d.getSeconds() + "&city_id="
            + $inputs.city_id.val() + "&city=" + $inputs.city.val();
    }
    $trutine.append("<ul>");
    for (var i in data.res) {
        var prenatal = new Date(data.res[i].prenatal);
        $trutine.append("<li>prenatal "+i
                +": <a href='"+DateToURL(prenatal)+"'>"
                + prenatal.toLocaleString() + "</a></li>");
        var natal = new Date(data.res[i].natal);
        $trutine.append("<li>natal "+i
                +": <a href='"+DateToURL(natal)+"'>"
                + natal.toLocaleString() +"</a></li>");
    }
    $trutine.append("</ul>");
}

function FetchPlanets(sky: Sky, query: string) {
    $.getJSON("soft?" + query, function (data) {
        RefreshChart(sky, data);
    });
    $.getJSON("algorithms?algo=trutine&" + query, function (data) {
        Trutine(data);
    });
}

function Go()
{
    var sky: Sky = new Sky();

    SetPanels();
    SetIncDateButton();
    SetDecDateButton();

    $inputs = {
        day: $("input[name='day']"),
        month: $("input[name='month']"),
        year: $("input[name='year']"),

        hour: $("input[name='hour']"),
        min: $("input[name='min']"),
        sec: $("input[name='sec']"),
        city_id: $("input[name='city_id']"),
        city: $("input[name='city']")
    };

    $("#catch").click(function(e) {
        if ($("input[name='city_id']").val() == -1) {
            $.Notify({
                caption: "Erreur",
                content: "Pas de ville sélectionnée",
                style: {background: 'red', foreground: 'white' },
                position: "top-right",
                width: 300,
                timeout: 1000,
                shadow: true
            });
            return false;
        }
        history.pushState(null, null, '?' + $("#coords").serialize());
        FetchPlanets(sky, $("#coords").serialize());
    });

    $("#go-derive").click(() => {
        var pos = sky.Asc.pos;
        sky.Asc.pos += ($("#derive-qtt").val() - 1) * 30;
        sky.Render();
        sky.Asc.pos= pos;
    });

    $.Notify({
        caption: "Problème ?",
        content: "N'oubliez pas de rafraîchir et de vider votre cache si "
            + "l'application ne fonctionne pas !",
        style: {background: 'blue', foreground: 'white' },
        position: "bottom-right",
        width: 300,
        timeout: 15000,
        shadow: true
    });

    $("#realtime").change(function() {
        rt();
    });

    var TheTpl = function(tpl) {
        return { render: function(ctx) {
            var res = tpl
                for (var k in ctx) {
                    res = res.replace("{{"+k+"}}", ctx[k]);
                }
            return res;
        }};
    }

    var MyTpl = { compile: function(tpl) { return TheTpl(tpl); }};

    $("input[name='city']").typeahead({
        name: 'cities',
        remote: "get_place?city=%QUERY",
        limit: 10,
        template: "<div class='bg-white listview-outlook'><a href='#' class='list'><div class='list-content'>{{value}}</div></a></div>",
        engine: MyTpl
    });

    $("input[name='city']").on('input', function() {
        $("input[name='city_id']").val(-1);
    });

    $("input[name='city']").on('typeahead:selected typeahead:autocompleted',
            function(ev, ...city: any[]){
                $("input[name='city_id']").val(city[0].id);
            });

    var params = getParams();

    if (!$.isEmptyObject(params)) {
        for (var k in params)
            $("input[name='" + k + "']").val(params[k]);

        $("#catch").click();
    }
    ShowStars(sky);
    ShowLots(sky);
}
