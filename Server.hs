{-# LANGUAGE OverloadedStrings, ScopedTypeVariables, FlexibleInstances #-}
module Main where

import Swehs.Ipl (IFlag(Sidereal))
import Swehs.Sweph
import Swehs.Date
import Control.Applicative ((<$>), (<*>), pure)
import Text.Read (readMaybe)
import Swehs.Sky
import Swehs.Geo
import Data.Aeson (encode, toJSON, Value (..))
import Database.HDBC
import Data.Text.Lazy
import Happstack.Lite
import Database.HDBC.Sqlite3
import qualified Text.Blaze.Html5 as H
import Control.Monad.Trans (MonadIO(liftIO))
import Data.Aeson.Encode as ABS
import Data.ByteString.Builder as B

instance ToMessage Value where
    toMessage = B.toLazyByteString . ABS.encodeToBuilder
    toContentType _ = "application/json"

badRequest = setResponseCode 400

main :: IO ()
main = serve (Just defaultServerConfig { port = 8000 }) myApp

myApp :: ServerPart Response
myApp = msum [ dir "get_place" $ getplace
                , dir "soft" $ soft
                , dir "fixstar" $ fixedStars
                , serveDirectory EnableBrowsing ["index.html"] "." ]

getplace :: ServerPart Response
getplace = do
        search <- lookText "city"
        res <- liftIO $ do
            db <- connectSqlite3 "geonames.sql"
            city <- findCityInfo db $ unpack search
            disconnect db
            return city
        ok . toResponse . encode $ res

soft :: ServerPart Response
soft = do
    date <- getTime
    cityIdMaybe <- lookVal "city_id"
    case (date, cityIdMaybe) of
        (Just d, Just cityId) -> do
                sky <- liftIO $ do
                    db <- connectSqlite3 "geonames.sql"
                    city <- findGeopos db cityId
                    disconnect db
                    withTZ <- applyTZ d city
                    let res = computeSky (toUT withTZ) city
                    return res
                ok . toResponse . toJSON $ sky
        _ -> do badRequest
                return . toResponse . pack $ "arguments ill formed: date:" ++ show date

fixedStars = do
        date <- getTime
        starMaybe <- lookStr "name"
        case (date, starMaybe) of
            (Just d, Just starName) -> do
                star <- liftIO $ return $ fixstar starName (julday d) [Sidereal]
                ok . toResponse . toJSON $ star
            _ -> do badRequest
                    return . toResponse . pack $ "invalid fixstar request"

applyTZ :: Gregorian -> Geopos -> IO Gregorian
applyTZ date@(Gregorian y m d t _) (Geopos _ _ zone) = do
            tzdb <- connectSqlite3 "tz.db"
            o <- findOffset tzdb date zone
            disconnect tzdb
            let res = Gregorian y m d t (TZ o)
            return res

getTime :: ServerPart (Maybe Gregorian)
getTime = do
    day <- lookVal "day"
    month <- lookVal "month"
    year <- lookVal "year"
    hour <- lookVal "hour"
    min <- lookVal "min"
    sec <- lookVal "sec"
    return $
        Gregorian <$> year <*> month <*> day <*>
            (time <$> hour <*> min <*> sec)
            <*> pure (TZ 0)
        where
            time hour min sec = hour + min / 60 + sec / 3600

lookVal :: Read a => String -> ServerPart (Maybe a)
lookVal arg = do
    arg <- lookStr arg
    return $ (arg >>= readMaybe)

lookStr :: String -> ServerPart (Maybe String)
lookStr arg = do
    txt <- unpack <$> lookText arg
    if Prelude.null txt then
        return Nothing
    else
        return $ Just txt

