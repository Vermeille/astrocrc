var Lots = [
    {
      id: 'L2',
      name: 'Accusation (aitiakos), Affliction (kakohtikos)',
      src: 'Tous les auteurs',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Saturn',
      nightFormula: 'Asc - Mars + Saturn'
    },
    {
      id: 'L3',
      name: 'Accusation (aitiakos), Affliction (kakohtikos)',
      src: 'Firmicus Maternus',
      fetch: function(sky) {
        return (sky.Asc.pos + sky.planets[Planets.Saturn].pos - sky.planets[Planets.Mars].pos + 360) % 360;
      },
      dayFormula: 'Asc + Saturn - Mars',
      nightFormula: 'Asc + Saturn - Mars'
    },
    {
      id: 'L4',
      name: 'Action (praxis)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Moon].pos - sky.planets[Planets.Mercury].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Moon].pos + sky.planets[Planets.Mercury].pos + 360) % 360;
      },
      dayFormula: 'Asc + Moon - Mercury',
      nightFormula: 'Asc - Moon + Mercury'
    },
    {
      id: 'L5',
      name: 'Action (praxis)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Mercury].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Mercury].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Mercury',
      nightFormula: 'Asc - Mars + Mercury'
    },
    {
      id: 'L6',
      name: 'Adultère (moicheia)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Venus].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Venus].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Venus',
      nightFormula: 'Asc - Mars + Venus'
    },
    {
      id: 'L7',
      name: 'Zone, Pays (chohrion)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mercury].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mercury].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mercury - Saturn',
      nightFormula: 'Asc - Mercury + Saturn'
    },
    {
      id: 'L8',
      name: 'Autorité (exousia)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Sun].pos - sky.planets[Planets.Mars].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Sun].pos + sky.planets[Planets.Mars].pos + 360) % 360;
      },
      dayFormula: 'Asc + Sun - Mars',
      nightFormula: 'Asc - Sun + Mars'
    },
    {
      id: 'L9',
      name: 'Base (basis)',
      src: 'Tous les auteurs',
      fetch: function(sky) {
        return (sky.Asc.pos + Lots[68].fetch(sky) - Lots[143].fetch(sky) + 360) % 360;
      },
      dayFormula: 'Asc + Fortune - Spirit',
      nightFormula: 'Asc + Fortune - Spirit'
    },
    {
      id: 'L10',
      name: 'Base (basis)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mercury].pos - sky.planets[Planets.Venus].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mercury].pos + sky.planets[Planets.Venus].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mercury - Venus',
      nightFormula: 'Asc - Mercury + Venus'
    },
    {
      id: 'L11',
      name: 'Commencement (archeh)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Venus].pos - sky.planets[Planets.Jupiter].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Venus].pos + sky.planets[Planets.Jupiter].pos + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Jupiter',
      nightFormula: 'Asc - Venus + Jupiter'
    },
    {
      id: 'L12',
      name: 'Excellence du Corps',
      src: 'Firmicus Maternus',
      fetch: function(sky) {
        return (sky.Asc.pos + sky.planets[Planets.Jupiter].pos - sky.planets[Planets.Moon].pos + 360) % 360;
      },
      dayFormula: 'Asc + Jupiter - Moon',
      nightFormula: 'Asc + Jupiter - Moon'
    },
    {
      id: 'L13',
      name: 'Enterrement (taphos)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Saturn].pos - sky.planets[Planets.Moon].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Saturn].pos + sky.planets[Planets.Moon].pos + 360) % 360;
      },
      dayFormula: 'Asc + Saturn - Moon',
      nightFormula: 'Asc - Saturn + Moon'
    },
    {
      id: 'L14',
      name: 'Business (pragma, pragmateia)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Saturn].pos - sky.planets[Planets.Jupiter].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Saturn].pos + sky.planets[Planets.Jupiter].pos + 360) % 360;
      },
      dayFormula: 'Asc + Saturn - Jupiter',
      nightFormula: 'Asc - Saturn + Jupiter'
    },
    {
      id: 'L15',
      name: 'Business (pragma, pragmateia)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Jupiter].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Jupiter].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Jupiter',
      nightFormula: 'Asc - Mars + Jupiter'
    },
    {
      id: 'L16',
      name: 'Changement, Tournant (tropeh)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (30 * 4 + sky.planets[Planets.Moon].pos - sky.planets[Planets.Sun].pos + 360) % 360;
        else
          return (30 * 4 - sky.planets[Planets.Moon].pos + sky.planets[Planets.Sun].pos + 360) % 360;
      },
      dayFormula: 'Leo + Moon - Sun',
      nightFormula: 'Leo - Moon + Sun'
    },
    {
      id: 'L17',
      name: 'Changement, Tournant (tropeh)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (30 * 3 + sky.planets[Planets.Sun].pos - sky.planets[Planets.Moon].pos + 360) % 360;
        else
          return (30 * 3 - sky.planets[Planets.Sun].pos + sky.planets[Planets.Moon].pos + 360) % 360;
      },
      dayFormula: 'Cancer + Sun - Moon',
      nightFormula: 'Cancer - Sun + Moon'
    },
    {
      id: 'L18',
      name: 'Enfants',
      src: 'Dorothée de Sidon',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Saturn].pos - sky.planets[Planets.Jupiter].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Saturn].pos + sky.planets[Planets.Jupiter].pos + 360) % 360;
      },
      dayFormula: 'Asc + Saturn - Jupiter',
      nightFormula: 'Asc - Saturn + Jupiter'
    },
    {
      id: 'L19',
      name: 'Enfants',
      src: 'Paulus Alexandrinus',
      fetch: function(sky) {
        return (sky.Asc.pos + sky.planets[Planets.Saturn].pos - sky.planets[Planets.Jupiter].pos + 360) % 360;
      },
      dayFormula: 'Asc + Saturn - Jupiter',
      nightFormula: 'Asc + Saturn - Jupiter'
    },
    {
      id: 'L20',
      name: 'Enfants',
      src: 'Firmicus Maternus',
      fetch: function(sky) {
        return (sky.Asc.pos + sky.planets[Planets.Mercury].pos - sky.planets[Planets.Venus].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mercury - Venus',
      nightFormula: 'Asc + Mercury - Venus'
    },
    {
      id: 'L21',
      name: 'Enfants',
      src: 'Firmicus Maternus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mercury].pos - sky.planets[Planets.Jupiter].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mercury].pos + sky.planets[Planets.Jupiter].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mercury - Jupiter',
      nightFormula: 'Asc - Mercury + Jupiter'
    },
    {
      id: 'L22',
      name: 'Enfants, Garçons',
      src: 'Vettius Valens',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mercury].pos - sky.planets[Planets.Jupiter].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mercury].pos + sky.planets[Planets.Jupiter].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mercury - Jupiter',
      nightFormula: 'Asc - Mercury + Jupiter'
    },
    {
      id: 'L23',
      name: 'Enfants, Garçons',
      src: 'Dorothée de Sidon',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Sun].pos - sky.planets[Planets.Jupiter].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Sun].pos + sky.planets[Planets.Jupiter].pos + 360) % 360;
      },
      dayFormula: 'Asc + Sun - Jupiter',
      nightFormula: 'Asc - Sun + Jupiter'
    },
    {
      id: 'L24',
      name: 'Enfants, Garçons',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Jupiter].pos - sky.planets[Planets.Moon].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Jupiter].pos + sky.planets[Planets.Moon].pos + 360) % 360;
      },
      dayFormula: 'Asc + Jupiter - Moon',
      nightFormula: 'Asc - Jupiter + Moon'
    },
    {
      id: 'L25',
      name: 'Enfants, Filles',
      src: 'Dorothée de Sidon',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Venus].pos - sky.planets[Planets.Moon].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Venus].pos + sky.planets[Planets.Moon].pos + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Moon',
      nightFormula: 'Asc - Venus + Moon'
    },
    {
      id: 'L26',
      name: 'Enfants, Filles',
      src: 'Vettius Valens',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Venus].pos - sky.planets[Planets.Jupiter].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Venus].pos + sky.planets[Planets.Jupiter].pos + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Jupiter',
      nightFormula: 'Asc - Venus + Jupiter'
    },
    {
      id: 'L27',
      name: 'Temps des Enfants',
      src: 'Dorothée de Sidon',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Jupiter].pos - sky.planets[Planets.Mars].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Jupiter].pos + sky.planets[Planets.Mars].pos + 360) % 360;
      },
      dayFormula: 'Asc + Jupiter - Mars',
      nightFormula: 'Asc - Jupiter + Mars'
    },
    {
      id: 'L28',
      name: 'Communauté, Société, Partenariat (koinohnia)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Venus].pos - sky.planets[Planets.Jupiter].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Venus].pos + sky.planets[Planets.Jupiter].pos + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Jupiter',
      nightFormula: 'Asc - Venus + Jupiter'
    },
    {
      id: 'L29',
      name: 'Communauté, Société, Partenariat (koinohnia)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Jupiter].pos - sky.planets[Planets.Mercury].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Jupiter].pos + sky.planets[Planets.Mercury].pos + 360) % 360;
      },
      dayFormula: 'Asc + Jupiter - Mercury',
      nightFormula: 'Asc - Jupiter + Mercury'
    },
    {
      id: 'L30',
      name: 'Communauté, Société, Partenariat (koinohnia)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Venus].pos - sky.planets[Planets.Mercury].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Venus].pos + sky.planets[Planets.Mercury].pos + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Mercury',
      nightFormula: 'Asc - Venus + Mercury'
    },
    {
      id: 'L31',
      name: 'Confinement, Contrainte (sunocheh)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Sun].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Sun].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Sun - Saturn',
      nightFormula: 'Asc - Sun + Saturn'
    },
    {
      id: 'L32',
      name: 'Confinement, Contrainte (sunocheh)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Saturn',
      nightFormula: 'Asc - Mars + Saturn'
    },
    {
      id: 'L33',
      name: 'Courage (tolmeh)',
      src: 'Tous les auteurs',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + Lots[68].fetch(sky) - sky.planets[Planets.Mars].pos + 360) % 360;
        else
          return (sky.Asc.pos - Lots[68].fetch(sky) + sky.planets[Planets.Mars].pos + 360) % 360;
      },
      dayFormula: 'Asc + Fortune - Mars',
      nightFormula: 'Asc - Fortune + Mars'
    },
    {
      id: 'L34',
      name: 'Accord, Testament (diathehkeh)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Jupiter].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Jupiter].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Jupiter - Saturn',
      nightFormula: 'Asc - Jupiter + Saturn'
    },
    {
      id: 'L35',
      name: 'Accord, Testament (diathehkeh)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Saturn].pos - sky.planets[Planets.Mercury].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Saturn].pos + sky.planets[Planets.Mercury].pos + 360) % 360;
      },
      dayFormula: 'Asc + Saturn - Mercury',
      nightFormula: 'Asc - Saturn + Mercury'
    },
    {
      id: 'L36',
      name: 'Accord, Testament (diathehkeh)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Mercury].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Mercury].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Mercury',
      nightFormula: 'Asc - Mars + Mercury'
    },
    {
      id: 'L37',
      name: 'Critère, Jugement Légal (kritehrion)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Saturn',
      nightFormula: 'Asc - Mars + Saturn'
    },
    {
      id: 'L38',
      name: 'Critère, Jugement Légal (kritehrion)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Jupiter].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Jupiter].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Jupiter - Saturn',
      nightFormula: 'Asc - Jupiter + Saturn'
    },
    {
      id: 'L39',
      name: 'Critère, Jugement Légal (kritehrion)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mercury].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mercury].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mercury - Saturn',
      nightFormula: 'Asc - Mercury + Saturn'
    },
    {
      id: 'L40',
      name: 'Mort (thanatos)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Moon].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Moon].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Moon - Saturn',
      nightFormula: 'Asc - Moon + Saturn'
    },
    {
      id: 'L41',
      name: 'Mort (thanatos)',
      src: 'Dorothée de Sidon et tous les auteurs',
      fetch: function(sky) {
        return (sky.planets[Planets.Saturn].pos + PosOfHouse(sky, 8) - sky.planets[Planets.Moon].pos + 360) % 360;
      },
      dayFormula: 'Saturn + 8ème signe - Moon',
      nightFormula: 'Saturn + 8ème signe - Moon'
    },
    {
      id: 'L42',
      name: 'Dette, prêts',
      src: 'Vettius Valens',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Saturn].pos - sky.planets[Planets.Mercury].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Saturn].pos + sky.planets[Planets.Mercury].pos + 360) % 360;
      },
      dayFormula: 'Asc + Saturn - Mercury',
      nightFormula: 'Asc - Saturn + Mercury'
    },
    {
      id: 'L43',
      name: 'Débiteur',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mercury].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mercury].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mercury - Saturn',
      nightFormula: 'Asc - Mercury + Saturn'
    },
    {
      id: 'L44',
      name: 'Desir (epithumia)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Venus].pos - sky.planets[Planets.Jupiter].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Venus].pos + sky.planets[Planets.Jupiter].pos + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Jupiter',
      nightFormula: 'Asc - Venus + Jupiter'
    },
    {
      id: 'L45',
      name: 'Destructeur (anairetehs)',
      src: 'Commentaire de Paulus Alexandrinus, Rhetorius et tous les auteurs',
      fetch: function(sky) {
        return (sky.Asc.pos + sky.planets[Planets.Moon].pos - MasterOf(sky, sky.Asc).pos + 360) % 360;
      },
      dayFormula: 'Asc + Moon - MasterOf Asc',
      nightFormula: 'Asc + Moon - MasterOf Asc'
    },
    {
      id: 'L46',
      name: 'Destructeur (anairetehs)',
      src: 'Rhetorius l\'Egyptien',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Moon].pos - MasterOf(sky, sky.Asc).pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Moon].pos + MasterOf(sky, sky.Asc).pos + 360) % 360;
      },
      dayFormula: 'Asc + Moon - MasterOf Asc',
      nightFormula: 'Asc - Moon + MasterOf Asc'
    },
    {
      id: 'L47',
      name: 'Destruction (apohleia)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mercury].pos - sky.planets[Planets.Mars].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mercury].pos + sky.planets[Planets.Mars].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mercury - Mars',
      nightFormula: 'Asc - Mercury + Mars'
    },
    {
      id: 'L48',
      name: 'Dignité (axiohma)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Sun].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Sun].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Sun',
      nightFormula: 'Asc - Mars + Sun'
    },
    {
      id: 'L49',
      name: 'Dignité (axiohma)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Sun].pos - sky.planets[Planets.Jupiter].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Sun].pos + sky.planets[Planets.Jupiter].pos + 360) % 360;
      },
      dayFormula: 'Asc + Sun - Jupiter',
      nightFormula: 'Asc - Sun + Jupiter'
    },
    {
      id: 'L50',
      name: 'Découverte (heurehma)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Jupiter].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Jupiter].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Jupiter - Saturn',
      nightFormula: 'Asc - Jupiter + Saturn'
    },
    {
      id: 'L51',
      name: 'Découverte (heurehma)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mercury].pos - sky.planets[Planets.Venus].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mercury].pos + sky.planets[Planets.Venus].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mercury - Venus',
      nightFormula: 'Asc - Mercury + Venus'
    },
    {
      id: 'L52',
      name: 'Rêves',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mercury].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mercury].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mercury - Saturn',
      nightFormula: 'Asc - Mercury + Saturn'
    },
    {
      id: 'L53',
      name: 'Ennemis',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mercury].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mercury].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mercury - Saturn',
      nightFormula: 'Asc - Mercury + Saturn'
    },
    {
      id: 'L54',
      name: 'Ennemis',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Saturn',
      nightFormula: 'Asc - Mars + Saturn'
    },
    {
      id: 'L55',
      name: 'Ennemis',
      src: 'Firmicus Maternus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mercury].pos - sky.planets[Planets.Mars].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mercury].pos + sky.planets[Planets.Mars].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mercury - Mars',
      nightFormula: 'Asc - Mercury + Mars'
    },
    {
      id: 'L56',
      name: 'Eros (Paulus)',
      src: 'Tous les auteurs',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Venus].pos - Lots[143].fetch(sky) + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Venus].pos + Lots[143].fetch(sky) + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Spirit',
      nightFormula: 'Asc - Venus + Spirit'
    },
    {
      id: 'L57',
      name: 'Eros (erohs)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Saturn',
      nightFormula: 'Asc - Mars + Saturn'
    },
    {
      id: 'L58',
      name: 'Eros (erohs)',
      src: 'Firmicus Maternus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + Lots[68].fetch(sky) - Lots[143].fetch(sky) + 360) % 360;
        else
          return (sky.Asc.pos - Lots[68].fetch(sky) + Lots[143].fetch(sky) + 360) % 360;
      },
      dayFormula: 'Asc + Fortune - Spirit',
      nightFormula: 'Asc - Fortune + Spirit'
    },
    {
      id: 'L59',
      name: 'Exaltation (hupsoma)',
      src: 'Tous les auteurs',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + 19 - sky.planets[Planets.Sun].pos + 360) % 360;
        else
          return (sky.Asc.pos - 19 + sky.planets[Planets.Sun].pos + 360) % 360;
      },
      dayFormula: 'Asc + 19 Bélier - Sun',
      nightFormula: 'Asc - 19 Bélier + Sun'
    },
    {
      id: 'L60',
      name: 'Expedition, Campagne (strateia)',
      src: 'Firmicus Maternus',
      fetch: function(sky) {
        return (sky.Asc.pos + sky.planets[Planets.Sun].pos - sky.planets[Planets.Mars].pos + 360) % 360;
      },
      dayFormula: 'Asc + Sun - Mars',
      nightFormula: 'Asc + Sun - Mars'
    },
    {
      id: 'L61',
      name: 'Expedition, Campagne (strateia)',
      src: 'Dorothée de Sidon et tous les auteurs',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Moon].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Moon].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Moon - Saturn',
      nightFormula: 'Asc - Moon + Saturn'
    },
    {
      id: 'L62',
      name: 'Expedition, Campagne (strateia)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Jupiter].pos - sky.planets[Planets.Mars].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Jupiter].pos + sky.planets[Planets.Mars].pos + 360) % 360;
      },
      dayFormula: 'Asc + Jupiter - Mars',
      nightFormula: 'Asc - Jupiter + Mars'
    },
    {
      id: 'L63',
      name: 'Expedition, Campagne (strateia)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Mercury].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Mercury].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Mercury',
      nightFormula: 'Asc - Mars + Mercury'
    },
    {
      id: 'L64',
      name: 'Agriculture',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Venus].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Venus].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Saturn',
      nightFormula: 'Asc - Venus + Saturn'
    },
    {
      id: 'L65',
      name: 'Père',
      src: 'Dorotheus, Paulus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Saturn].pos - sky.planets[Planets.Sun].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Saturn].pos + sky.planets[Planets.Sun].pos + 360) % 360;
      },
      dayFormula: 'Asc + Saturn - Sun',
      nightFormula: 'Asc - Saturn + Sun'
    },
    {
      id: 'L66',
      name: 'Père',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Saturn].pos - sky.planets[Planets.Mercury].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Saturn].pos + sky.planets[Planets.Mercury].pos + 360) % 360;
      },
      dayFormula: 'Asc + Saturn - Mercury',
      nightFormula: 'Asc - Saturn + Mercury'
    },
    {
      id: 'L67',
      name: 'Pères, Patrie (patris)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Jupiter].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Jupiter].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Jupiter - Saturn',
      nightFormula: 'Asc - Jupiter + Saturn'
    },
    {
      id: 'L68',
      name: 'Pères, Patrie (patris)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mercury].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mercury].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mercury - Saturn',
      nightFormula: 'Asc - Mercury + Saturn'
    },
    {
      id: 'L69',
      name: 'Pères, Patrie (patris)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Sun].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Sun].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Sun',
      nightFormula: 'Asc - Mars + Sun'
    },
    {
      id: 'L70',
      name: 'Fortune (tucheh)',
      src: 'Tous les auteurs',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Moon].pos - sky.planets[Planets.Sun].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Moon].pos + sky.planets[Planets.Sun].pos + 360) % 360;
      },
      dayFormula: 'Asc + Moon - Sun',
      nightFormula: 'Asc - Moon + Sun'
    },
    {
      id: 'L71',
      name: 'Fortune (tucheh)',
      src: 'Ptolémée',
      fetch: function(sky) {
        return (sky.Asc.pos + sky.planets[Planets.Moon].pos - sky.planets[Planets.Sun].pos + 360) % 360;
      },
      dayFormula: 'Asc + Moon - Sun',
      nightFormula: 'Asc + Moon - Sun'
    },
    {
      id: 'L72',
      name: 'Liberté (eleutheria)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Sun].pos - sky.planets[Planets.Mercury].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Sun].pos + sky.planets[Planets.Mercury].pos + 360) % 360;
      },
      dayFormula: 'Asc + Sun - Mercury',
      nightFormula: 'Asc - Sun + Mercury'
    },
    {
      id: 'L73',
      name: 'Amie d\'un homme',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Jupiter].pos - sky.planets[Planets.Moon].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Jupiter].pos + sky.planets[Planets.Moon].pos + 360) % 360;
      },
      dayFormula: 'Asc + Jupiter - Moon',
      nightFormula: 'Asc - Jupiter + Moon'
    },
    {
      id: 'L74',
      name: 'Amie d\'une femme',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Moon].pos - sky.planets[Planets.Venus].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Moon].pos + sky.planets[Planets.Venus].pos + 360) % 360;
      },
      dayFormula: 'Asc + Moon - Venus',
      nightFormula: 'Asc - Moon + Venus'
    },
    {
      id: 'L75',
      name: 'Ami d\'un homme',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Jupiter].pos - sky.planets[Planets.Venus].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Jupiter].pos + sky.planets[Planets.Venus].pos + 360) % 360;
      },
      dayFormula: 'Asc + Jupiter - Venus',
      nightFormula: 'Asc - Jupiter + Venus'
    },
    {
      id: 'L76',
      name: 'Ami d\'une femme',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Moon].pos - sky.planets[Planets.Jupiter].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Moon].pos + sky.planets[Planets.Jupiter].pos + 360) % 360;
      },
      dayFormula: 'Asc + Moon - Jupiter',
      nightFormula: 'Asc - Moon + Jupiter'
    },
    {
      id: 'L77',
      name: 'Amis',
      src: 'Firmicus Maternus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mercury].pos - sky.planets[Planets.Jupiter].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mercury].pos + sky.planets[Planets.Jupiter].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mercury - Jupiter',
      nightFormula: 'Asc - Mercury + Jupiter'
    },
    {
      id: 'L78',
      name: 'Amis',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Venus].pos - sky.planets[Planets.Jupiter].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Venus].pos + sky.planets[Planets.Jupiter].pos + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Jupiter',
      nightFormula: 'Asc - Venus + Jupiter'
    },
    {
      id: 'L79',
      name: 'Amis',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Venus].pos - sky.planets[Planets.Moon].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Venus].pos + sky.planets[Planets.Moon].pos + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Moon',
      nightFormula: 'Asc - Venus + Moon'
    },
    {
      id: 'L80',
      name: 'Amis',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mercury].pos - sky.planets[Planets.Moon].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mercury].pos + sky.planets[Planets.Moon].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mercury - Moon',
      nightFormula: 'Asc - Mercury + Moon'
    },
    {
      id: 'L81',
      name: 'Amis',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Venus].pos - sky.planets[Planets.Mercury].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Venus].pos + sky.planets[Planets.Mercury].pos + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Mercury',
      nightFormula: 'Asc - Venus + Mercury'
    },
    {
      id: 'L82',
      name: 'Habitation (oikehsis)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Moon].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Moon].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Moon - Saturn',
      nightFormula: 'Asc - Moon + Saturn'
    },
    {
      id: 'L83',
      name: 'Haine (misos)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Saturn].pos - sky.planets[Planets.Mars].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Saturn].pos + sky.planets[Planets.Mars].pos + 360) % 360;
      },
      dayFormula: 'Asc + Saturn - Mars',
      nightFormula: 'Asc - Saturn + Mars'
    },
    {
      id: 'L84',
      name: 'Honneur',
      src: 'Firmicus Maternus',
      fetch: function(sky) {
        return (sky.Asc.pos + sky.planets[Planets.Sun].pos - PosOfHouse(sky, 9) + 360) % 360;
      },
      dayFormula: 'Asc + Sun - Sign 9',
      nightFormula: 'Asc + Sun - Sign 9'
    },
    {
      id: 'L85',
      name: 'Informer, Mettre en lumière (mehnutehs)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Venus].pos - sky.planets[Planets.Mercury].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Venus].pos + sky.planets[Planets.Mercury].pos + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Mercury',
      nightFormula: 'Asc - Venus + Mercury'
    },
    {
      id: 'L86',
      name: 'Succession',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Venus].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Venus].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Saturn',
      nightFormula: 'Asc - Venus + Saturn'
    },
    {
      id: 'L87',
      name: 'Succession',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mercury].pos - sky.planets[Planets.Jupiter].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mercury].pos + sky.planets[Planets.Jupiter].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mercury - Jupiter',
      nightFormula: 'Asc - Mercury + Jupiter'
    },
    {
      id: 'L88',
      name: 'Blessure, Santé ou Maladie',
      src: 'Dorotheus, Maternus, et al',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Saturn',
      nightFormula: 'Asc - Mars + Saturn'
    },
    {
      id: 'L89',
      name: 'Blessure, Santé ou Maladie',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Saturn].pos - sky.planets[Planets.Mars].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Saturn].pos + sky.planets[Planets.Mars].pos + 360) % 360;
      },
      dayFormula: 'Asc + Saturn - Mars',
      nightFormula: 'Asc - Saturn + Mars'
    },
    {
      id: 'L90',
      name: 'Blessure, Santé ou Maladie',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.planets[Planets.Mercury].pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.planets[Planets.Mercury].pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Mercury + Mars - Saturn',
      nightFormula: 'Mercury - Mars + Saturn'
    },
    {
      id: 'L91',
      name: 'Justice (dikeh)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Saturn].pos - sky.planets[Planets.Mercury].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Saturn].pos + sky.planets[Planets.Mercury].pos + 360) % 360;
      },
      dayFormula: 'Asc + Saturn - Mercury',
      nightFormula: 'Asc - Saturn + Mercury'
    },
    {
      id: 'L92',
      name: 'Royaume (basileia)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
            return 0;//(MC + sky.planets[Planets.Moon].pos - sky.planets[Planets.Sun].pos + 360) % 360;
        else
            return 0;//(MC - sky.planets[Planets.Moon].pos + sky.planets[Planets.Sun].pos + 360) % 360;
      },
      dayFormula: 'MC + Moon - Sun',
      nightFormula: 'MC - Moon + Sun'
    },
    {
      id: 'L93',
      name: 'Terres, Propriété foncière (eggaios)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Venus].pos - sky.planets[Planets.Mars].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Venus].pos + sky.planets[Planets.Mars].pos + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Mars',
      nightFormula: 'Asc - Venus + Mars'
    },
    {
      id: 'L94',
      name: 'Terres, Propriété foncière (eggaios)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Venus].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Venus].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Venus',
      nightFormula: 'Asc - Mars + Venus'
    },
    {
      id: 'L95',
      name: 'Terres, Propriété foncière (eggaios)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Saturn].pos - sky.planets[Planets.Venus].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Saturn].pos + sky.planets[Planets.Venus].pos + 360) % 360;
      },
      dayFormula: 'Asc + Saturn - Venus',
      nightFormula: 'Asc - Saturn + Venus'
    },
    {
      id: 'L96',
      name: 'Terres, Propriété foncière (eggaios)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        return (sky.Asc.pos + sky.planets[Planets.Saturn].pos - sky.planets[Planets.Venus].pos + 360) % 360;
      },
      dayFormula: 'Asc + Saturn - Venus',
      nightFormula: 'Asc + Saturn - Venus'
    },
    {
      id: 'L97',
      name: 'Prêteur d\'Argent (daneistehs)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        return (sky.Asc.pos + sky.planets[Planets.Mercury].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mercury - Saturn',
      nightFormula: 'Asc + Mercury - Saturn'
    },
    {
      id: 'L98',
      name: 'Vie (zoheh)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Venus].pos - sky.planets[Planets.Moon].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Venus].pos + sky.planets[Planets.Moon].pos + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Moon',
      nightFormula: 'Asc - Venus + Moon'
    },
    {
      id: 'L99',
      name: 'Vie (zoheh)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Saturn].pos - sky.planets[Planets.Jupiter].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Saturn].pos + sky.planets[Planets.Jupiter].pos + 360) % 360;
      },
      dayFormula: 'Asc + Saturn - Jupiter',
      nightFormula: 'Asc - Saturn + Jupiter'
    },
    {
      id: 'L100',
      name: 'Vie (zoheh)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Saturn].pos - sky.planets[Planets.Venus].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Saturn].pos + sky.planets[Planets.Venus].pos + 360) % 360;
      },
      dayFormula: 'Asc + Saturn - Venus',
      nightFormula: 'Asc - Saturn + Venus'
    },
    {
      id: 'L101',
      name: 'Vie (zoheh)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mercury].pos - sky.planets[Planets.Venus].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mercury].pos + sky.planets[Planets.Venus].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mercury - Venus',
      nightFormula: 'Asc - Mercury + Venus'
    },
    {
      id: 'L102',
      name: 'Vie (zoheh)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Venus].pos - Lots[68].fetch(sky) + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Venus].pos + Lots[68].fetch(sky) + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Fortune',
      nightFormula: 'Asc - Venus + Fortune'
    },
    {
      id: 'L103',
      name: 'Gagne-pain (bios)',
      src: 'Dorothée de Sidon et tout son club',
      fetch: function(sky) {
        return (sky.Asc.pos + PosOfHouse(sky, 2) - MasterOfHouse(sky, 2).pos + 360) % 360;
      },
      dayFormula: 'Asc + 2ème - Maître de 2ème',
      nightFormula: 'Asc + 2ème - Maître de 2ème'
    },
    {
      id: 'L104',
      name: 'Gagne-pain (bios)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Saturn].pos - sky.planets[Planets.Venus].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Saturn].pos + sky.planets[Planets.Venus].pos + 360) % 360;
      },
      dayFormula: 'Asc + Saturn - Venus',
      nightFormula: 'Asc - Saturn + Venus'
    },
    {
      id: 'L105',
      name: 'Gagne-pain (bios)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Moon].pos - sky.planets[Planets.Venus].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Moon].pos + sky.planets[Planets.Venus].pos + 360) % 360;
      },
      dayFormula: 'Asc + Moon - Venus',
      nightFormula: 'Asc - Moon + Venus'
    },
    {
      id: 'L106',
      name: 'Gagne-pain (bios)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.Asc.pos - sky.planets[Planets.Sun].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.Asc.pos + sky.planets[Planets.Sun].pos + 360) % 360;
      },
      dayFormula: 'Asc + Asc - Sun',
      nightFormula: 'Asc - Asc + Sun'
    },
    {
      id: 'L107',
      name: 'Perte (zehmia)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Saturn].pos - sky.planets[Planets.Mars].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Saturn].pos + sky.planets[Planets.Mars].pos + 360) % 360;
      },
      dayFormula: 'Asc + Saturn - Mars',
      nightFormula: 'Asc - Saturn + Mars'
    },
    {
      id: 'L108',
      name: 'Joie du Mariage',
      src: 'Dorothée de Sidon',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + 30 * 6 - sky.planets[Planets.Venus].pos + 360) % 360;
        else
          return (sky.Asc.pos - 30 * 6 + sky.planets[Planets.Venus].pos + 360) % 360;
      },
      dayFormula: 'Asc + 7ème signe - Venus',
      nightFormula: 'Asc - 7ème signe + Venus'
    },
    {
      id: 'L109',
      name: 'Mariage, Hommes',
      src: 'Paulus Alexandrinus',
      fetch: function(sky) {
        return (sky.Asc.pos + sky.planets[Planets.Venus].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Saturn',
      nightFormula: 'Asc + Venus - Saturn'
    },
    {
      id: 'L110',
      name: 'Mariage, Hommes',
      src: 'Vettius Valens',
      fetch: function(sky) {
        return (sky.Asc.pos + sky.planets[Planets.Venus].pos - sky.planets[Planets.Sun].pos + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Sun',
      nightFormula: 'Asc + Venus - Sun'
    },
    {
      id: 'L111',
      name: 'Mariage, Hommes',
      src: 'Dorotheus, Maternus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Venus].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Venus].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Saturn',
      nightFormula: 'Asc - Venus + Saturn'
    },
    {
      id: 'L112',
      name: 'Mariage, Cérémonie',
      src: 'Dorothée de Sidon',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.planets[Planets.Venus].pos + sky.planets[Planets.Moon].pos - sky.planets[Planets.Sun].pos + 360) % 360;
        else
          return (sky.planets[Planets.Venus].pos - sky.planets[Planets.Moon].pos + sky.planets[Planets.Sun].pos + 360) % 360;
      },
      dayFormula: 'Venus + Moon - Sun',
      nightFormula: 'Venus - Moon + Sun'
    },
    {
      id: 'L113',
      name: 'Mariage, Femmes',
      src: 'Paulus Alexandrinus',
      fetch: function(sky) {
        return (sky.Asc.pos + sky.planets[Planets.Saturn].pos - sky.planets[Planets.Venus].pos + 360) % 360;
      },
      dayFormula: 'Asc + Saturn - Venus',
      nightFormula: 'Asc + Saturn - Venus'
    },
    {
      id: 'L114',
      name: 'Mariage, Femmes',
      src: 'Vettius Valens',
      fetch: function(sky) {
        return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Moon].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Moon',
      nightFormula: 'Asc + Mars - Moon'
    },
    {
      id: 'L115',
      name: 'Mariage, Femmes',
      src: 'Dorotheus, Maternus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Saturn].pos - sky.planets[Planets.Venus].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Saturn].pos + sky.planets[Planets.Venus].pos + 360) % 360;
      },
      dayFormula: 'Asc + Saturn - Venus',
      nightFormula: 'Asc - Saturn + Venus'
    },
    {
      id: 'L116',
      name: 'Mariage, Femmes',
      src: 'Firmicus Maternus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Venus].pos - sky.planets[Planets.Mars].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Venus].pos + sky.planets[Planets.Mars].pos + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Mars',
      nightFormula: 'Asc - Venus + Mars'
    },
    {
      id: 'L117',
      name: 'Mariage, général',
      src: 'Vettius Valens',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Venus].pos - sky.planets[Planets.Jupiter].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Venus].pos + sky.planets[Planets.Jupiter].pos + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Jupiter',
      nightFormula: 'Asc - Venus + Jupiter'
    },
    {
      id: 'L118',
      name: 'Mariage, général',
      src: 'Firmicus Maternus',
      fetch: function(sky) {
        return (sky.Asc.pos + sky.planets[Planets.Moon].pos - sky.planets[Planets.Sun].pos + 360) % 360;
      },
      dayFormula: 'Asc + Moon - Sun',
      nightFormula: 'Asc + Moon - Sun'
    },
    {
      id: 'L119',
      name: 'Modestie (aidehmohn)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Moon].pos - sky.planets[Planets.Mars].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Moon].pos + sky.planets[Planets.Mars].pos + 360) % 360;
      },
      dayFormula: 'Asc + Moon - Mars',
      nightFormula: 'Asc - Moon + Mars'
    },
    {
      id: 'L120',
      name: 'Mère',
      src: 'Tous les auteurs',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Moon].pos - sky.planets[Planets.Venus].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Moon].pos + sky.planets[Planets.Venus].pos + 360) % 360;
      },
      dayFormula: 'Asc + Moon - Venus',
      nightFormula: 'Asc - Moon + Venus'
    },
    {
      id: 'L121',
      name: 'Nécéssité (Paulus)',
      src: 'Tous les auteurs',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + Lots[68].fetch(sky) - sky.planets[Planets.Mercury].pos + 360) % 360;
        else
          return (sky.Asc.pos - Lots[68].fetch(sky) + sky.planets[Planets.Mercury].pos + 360) % 360;
      },
      dayFormula: 'Asc + Fortune - Mercury',
      nightFormula: 'Asc - Fortune + Mercury'
    },
    {
      id: 'L122',
      name: 'Némésis',
      src: 'Tous les auteurs',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + Lots[68].fetch(sky) - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - Lots[68].fetch(sky) + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Fortune - Saturn',
      nightFormula: 'Asc - Fortune + Saturn'
    },
    {
      id: 'L123',
      name: 'Douleur (lupeh)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Sun].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Sun].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Sun - Saturn',
      nightFormula: 'Asc - Sun + Saturn'
    },
    {
      id: 'L124',
      name: 'Douleur (lupeh)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Saturn',
      nightFormula: 'Asc - Mars + Saturn'
    },
    {
      id: 'L125',
      name: 'Parents',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Moon].pos - sky.planets[Planets.Sun].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Moon].pos + sky.planets[Planets.Sun].pos + 360) % 360;
      },
      dayFormula: 'Asc + Moon - Sun',
      nightFormula: 'Asc - Moon + Sun'
    },
    {
      id: 'L126',
      name: 'Possessions',
      src: 'Firmicus Maternus',
      fetch: function(sky) {
        return (sky.Asc.pos + sky.planets[Planets.Jupiter].pos - sky.planets[Planets.Mercury].pos + 360) % 360;
      },
      dayFormula: 'Asc + Jupiter - Mercury',
      nightFormula: 'Asc + Jupiter - Mercury'
    },
    {
      id: 'L127',
      name: 'Achats',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Venus].pos - sky.planets[Planets.Mars].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Venus].pos + sky.planets[Planets.Mars].pos + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Mars',
      nightFormula: 'Asc - Venus + Mars'
    },
    {
      id: 'L128',
      name: 'Achats',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Venus].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Venus].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Venus',
      nightFormula: 'Asc - Mars + Venus'
    },
    {
      id: 'L129',
      name: 'Compensation, Contrainte (anocheh)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Sun].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Sun].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Sun - Saturn',
      nightFormula: 'Asc - Sun + Saturn'
    },
    {
      id: 'L130',
      name: 'Réputation (doxa)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Venus].pos - sky.planets[Planets.Jupiter].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Venus].pos + sky.planets[Planets.Jupiter].pos + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Jupiter',
      nightFormula: 'Asc - Venus + Jupiter'
    },
    {
      id: 'L131',
      name: 'Réputation (doxa)',
      src: 'Firmicus Maternus',
      fetch: function(sky) {
        throw "MILIEU DU CIEL"; //return (sky.Asc.pos + Milieu du Ciel - sky.planets[Planets.Sun].pos + 360) % 360;
      },
      dayFormula: 'Asc + Milieu du Ciel - Sun',
      nightFormula: 'Asc + Milieu du Ciel - Sun'
    },
    {
      id: 'L132',
      name: 'Navigation',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Saturn].pos - 30 * 3 + 15 + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Saturn].pos + 30 * 3 + 15 + 360) % 360;
      },
      dayFormula: 'Asc + Saturn - [15?]Degrés du Cancer',
      nightFormula: 'Asc - Saturn + [15?]Degrés du Cancer'
    },
    {
      id: 'L133',
      name: 'Vente (prasis)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Jupiter].pos - sky.planets[Planets.Sun].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Jupiter].pos + sky.planets[Planets.Sun].pos + 360) % 360;
      },
      dayFormula: 'Asc + Jupiter - Sun',
      nightFormula: 'Asc - Jupiter + Sun'
    },
    {
      id: 'L134',
      name: 'Vente (prasis)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Venus].pos - sky.planets[Planets.Jupiter].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Venus].pos + sky.planets[Planets.Jupiter].pos + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Jupiter',
      nightFormula: 'Asc - Venus + Jupiter'
    },
    {
      id: 'L135',
      name: 'Vente (prasis)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Venus].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Venus].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Venus',
      nightFormula: 'Asc - Mars + Venus'
    },
    {
      id: 'L136',
      name: 'Voyage en mer, Aventure ou Entreprise',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Jupiter].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Jupiter].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Jupiter - Saturn',
      nightFormula: 'Asc - Jupiter + Saturn'
    },
    {
      id: 'L137',
      name: 'Voyage en mer, Aventure ou Entreprise',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mercury].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mercury].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mercury - Saturn',
      nightFormula: 'Asc - Mercury + Saturn'
    },
    {
      id: 'L138',
      name: 'Enfants (de même parents)',
      src: 'Dorotheus, Valens, Maternus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Jupiter].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Jupiter].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Jupiter - Saturn',
      nightFormula: 'Asc - Jupiter + Saturn'
    },
    {
      id: 'L139',
      name: 'Enfants (de même parents)',
      src: 'Paulus Alexandrinus',
      fetch: function(sky) {
        return (sky.Asc.pos + sky.planets[Planets.Jupiter].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Jupiter - Saturn',
      nightFormula: 'Asc + Jupiter - Saturn'
    },
    {
      id: 'L140',
      name: 'Nombre d\'enfants (de même parents)',
      src: 'Dorothée de Sidon',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Jupiter].pos - sky.planets[Planets.Mercury].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Jupiter].pos + sky.planets[Planets.Mercury].pos + 360) % 360;
      },
      dayFormula: 'Asc + Jupiter - Mercury',
      nightFormula: 'Asc - Jupiter + Mercury'
    },
    {
      id: 'L141',
      name: 'Esclaves',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Moon].pos - sky.planets[Planets.Mars].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Moon].pos + sky.planets[Planets.Mars].pos + 360) % 360;
      },
      dayFormula: 'Asc + Moon - Mars',
      nightFormula: 'Asc - Moon + Mars'
    },
    {
      id: 'L142',
      name: 'Esclaves',
      src: 'Hepaistos de Thèbes et tous les auteurs',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Moon].pos - sky.planets[Planets.Mercury].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Moon].pos + sky.planets[Planets.Mercury].pos + 360) % 360;
      },
      dayFormula: 'Asc + Moon - Mercury',
      nightFormula: 'Asc - Moon + Mercury'
    },
    {
      id: 'L143',
      name: 'Esclaves',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        return (sky.Asc.pos + sky.planets[Planets.Moon].pos - sky.planets[Planets.Mercury].pos + 360) % 360;
      },
      dayFormula: 'Asc + Moon - Mercury',
      nightFormula: 'Asc + Moon - Mercury'
    },
    {
      id: 'L144',
      name: 'Semis (sporimos)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Sun].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Sun].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Sun',
      nightFormula: 'Asc - Mars + Sun'
    },
    {
      id: 'L145',
      name: 'Esprit (daimohn)',
      src: 'Tous les auteurs',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Sun].pos - sky.planets[Planets.Moon].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Sun].pos + sky.planets[Planets.Moon].pos + 360) % 360;
      },
      dayFormula: 'Asc + Sun - Moon',
      nightFormula: 'Asc - Sun + Moon'
    },
    {
      id: 'L146',
      name: 'Corpulence, Force (eurohstia)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          throw "milieu ciel"; //return (sky.Asc.pos + Milieu du Ciel - sky.planets[Planets.Sun].pos + 360) % 360;
        else
          throw "milieu ciel"; //return (sky.Asc.pos - Milieu du Ciel + sky.planets[Planets.Sun].pos + 360) % 360;
      },
      dayFormula: 'Asc + Milieu du Ciel - Sun',
      nightFormula: 'Asc - Milieu du Ciel + Sun'
    },
    {
      id: 'L147',
      name: 'Technique (techneh)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Sun].pos - sky.planets[Planets.Mars].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Sun].pos + sky.planets[Planets.Mars].pos + 360) % 360;
      },
      dayFormula: 'Asc + Sun - Mars',
      nightFormula: 'Asc - Sun + Mars'
    },
    {
      id: 'L148',
      name: 'Technique (techneh)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Moon].pos - sky.planets[Planets.Mars].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Moon].pos + sky.planets[Planets.Mars].pos + 360) % 360;
      },
      dayFormula: 'Asc + Moon - Mars',
      nightFormula: 'Asc - Moon + Mars'
    },
    {
      id: 'L149',
      name: 'Technique (techneh)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Venus].pos - sky.planets[Planets.Mars].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Venus].pos + sky.planets[Planets.Mars].pos + 360) % 360;
      },
      dayFormula: 'Asc + Venus - Mars',
      nightFormula: 'Asc - Venus + Mars'
    },
    {
      id: 'L150',
      name: 'Vol, Voleurs',
      src: 'Vettius Valens',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.planets[Planets.Saturn].pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Mercury].pos + 360) % 360;
        else
          return (sky.planets[Planets.Saturn].pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Mercury].pos + 360) % 360;
      },
      dayFormula: 'Saturn + Mars - Mercury',
      nightFormula: 'Saturn - Mars + Mercury'
    },
    {
      id: 'L151',
      name: 'Vol, Voleurs',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Mercury].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Mercury].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Mercury',
      nightFormula: 'Asc - Mars + Mercury'
    },
    {
      id: 'L152',
      name: 'Voyage à l\'étranger (apodehmia), Exil (ekdehmia), Pays étranger (xenia)',
      src: 'Vettius Valens et tous les auteurs',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Saturn',
      nightFormula: 'Asc - Mars + Saturn'
    },
    {
      id: 'L153',
      name: 'Voyage à l\'étranger (apodehmia), Exil (ekdehmia), Pays étranger (xenia)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Sun].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Sun].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Sun',
      nightFormula: 'Asc - Mars + Sun'
    },
    {
      id: 'L154',
      name: 'Voyage à l\'étranger (apodehmia), Exil (ekdehmia), Pays étranger (xenia)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Mercury].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Mercury].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Mercury',
      nightFormula: 'Asc - Mars + Mercury'
    },
    {
      id: 'L155',
      name: 'Voyage à l\'étranger (apodehmia), Exil (ekdehmia), Pays étranger (xenia)',
      src: 'Firmicus Maternus',
      fetch: function(sky) {
        return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Sun].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Sun',
      nightFormula: 'Asc + Mars - Sun'
    },
    {
      id: 'L156',
      name: 'Voyage à l\'étranger (apodehmia), Exil (ekdehmia), Pays étranger (xenia)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        return (sky.Asc.pos + PosOfHouse(sky, 9) - MasterOfHouse(sky, 9).pos + 360) % 360;
      },
      dayFormula: 'Asc + 9ème - Maître de 9ème',
      nightFormula: 'Asc + 9ème - Maître de 9ème'
    },
    {
      id: 'L157',
      name: 'Trahison (enedra)',
      src: 'Vettius Valens et tous les auteurs',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Sun].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Sun].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Sun',
      nightFormula: 'Asc - Mars + Sun'
    },
    {
      id: 'L158',
      name: 'Tyran (despotehs)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        return (sky.Asc.pos + sky.planets[Planets.Sun].pos - sky.planets[Planets.Venus].pos + 360) % 360;
      },
      dayFormula: 'Asc + Sun - Venus',
      nightFormula: 'Asc + Sun - Venus'
    },
    {
      id: 'L159',
      name: 'Victoire (nikeh)',
      src: 'Tous les auteurs',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Jupiter].pos - Lots[143].fetch(sky) + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Jupiter].pos + Lots[143].fetch(sky) + 360) % 360;
      },
      dayFormula: 'Asc + Jupiter - Lot d\'Esprit',
      nightFormula: 'Asc - Jupiter + Lot d\'Esprit'
    },
    {
      id: 'L160',
      name: 'Victoire (nikeh)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mars].pos - sky.planets[Planets.Venus].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mars].pos + sky.planets[Planets.Venus].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mars - Venus',
      nightFormula: 'Asc - Mars + Venus'
    },
    {
      id: 'L161',
      name: 'Eaux (hudatos)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Saturn].pos - sky.planets[Planets.Venus].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Saturn].pos + sky.planets[Planets.Venus].pos + 360) % 360;
      },
      dayFormula: 'Asc + Saturn - Venus',
      nightFormula: 'Asc - Saturn + Venus'
    },
    {
      id: 'L162',
      name: 'Faiblesse, Maladie (arrohstia)',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Mercury].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Mercury].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Mercury - Saturn',
      nightFormula: 'Asc - Mercury + Saturn'
    },
    {
      id: 'L163',
      name: 'Richesse',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Sun].pos - sky.planets[Planets.Saturn].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Sun].pos + sky.planets[Planets.Saturn].pos + 360) % 360;
      },
      dayFormula: 'Asc + Sun - Saturn',
      nightFormula: 'Asc - Sun + Saturn'
    },
    {
      id: 'L164',
      name: 'Richesse',
      src: 'Commentaire de Paulus Alexandrinus',
      fetch: function(sky) {
        if (IsDiurnChart(sky))
          return (sky.Asc.pos + sky.planets[Planets.Sun].pos - sky.planets[Planets.Jupiter].pos + 360) % 360;
        else
          return (sky.Asc.pos - sky.planets[Planets.Sun].pos + sky.planets[Planets.Jupiter].pos + 360) % 360;
      },
      dayFormula: 'Asc + Sun - Jupiter',
      nightFormula: 'Asc - Sun + Jupiter'
    }
];

