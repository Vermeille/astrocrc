///<reference path="zodiac.ts"/>
///<reference path="jquery.d.ts"/>

declare function reinitTrees(): void;

function SetHighlights(plnt: Planets, col: string, sky: Sky): void {
    var total_col = "0px 0px 20px " + col;
    total_col += ", " + total_col;
    total_col += ", " + total_col;

    var $plnt = $("#" + Planets[plnt]);
    var $light_plnt = $(".light-" + Planets[plnt]);
    $plnt.unbind();
    $light_plnt.unbind();

    // <plnt> -> .light-<plnt>
    $plnt.hover(function () {
        $light_plnt.each(function () {
            $(this).css("text-shadow", total_col);
        });
        $plnt.css("text-shadow", total_col);

        for (var p of PlntIterator) {
            var asp = Aspect(sky, sky.planets[plnt], sky.planets[p]);
            if (plnt != p && asp != null) {
                LinkPlanets(sky, sky.planets[plnt], sky.planets[p], asp);
            }
        }
    });

    $plnt.mouseleave(function () {
        $light_plnt.each(function () {
            $(this).css("text-shadow", "0px 0px 0px " + col);
        });
        $plnt.css("text-shadow", "0px 0px 0px " + col);
        $("#lines").empty();
    });

    // .light-<plnt> -> <plnt>
    $light_plnt.hover(function () {
        $plnt.css("text-shadow", total_col);
        $(this).css("text-shadow", total_col);
    });

    $light_plnt.mouseleave(function () {
        $plnt.css("text-shadow", "0px 0px 0px " + col);
        $(this).css("text-shadow", "0px 0px 0px " + col);
    });
}

function MasterOf(sky: Sky, b: Body): Body {
    return sky.planets[Signs[b.sign].ruler];
}

function PosOfHouse(sky: Sky, h: number): number {
    return ((h - 1) * 30 + sky.Asc.sign * 30) % 360;
}

function MasterOfHouse(sky: Sky, h: number): Body {
    return sky.planets[Signs[Math.floor(PosOfHouse(sky, 2) / 30)].ruler];
}

function AspectToSym(asp: Aspects): string {
    switch (asp) {
        case Aspects.Conjunction:
            return "<span class='cls-sym'>!</span>";
        case Aspects.Opposition:
            return "<span class='cls-sym'>\"</span>";
        case Aspects.Square:
            return "<span class='cls-sym'>#</span>";
        case Aspects.Trine:
            return "<span class='cls-sym'>$</span>";
        case Aspects.Sextile:
            return "<span class='cls-sym'>'</span>";
    }
}

function RenderBodies(sky: Sky) {
    sky.Render();
}

function RefreshUI(sky: Sky) {
    RenderBodies(sky);

    $("#plaintext").text("Maitre de geniture: " + MasterGeniture(sky) + "\n");

    DrawResume(sky);
    SetupUI(sky);
}

function ShowStars(sky: Sky) {
    var list = "<ul style='-webkit-columns:3;' data-role='treeview' class='treeview'>";

    for (var c in FixedStars) {
        if (FixedStars[c].length > 0) {
            list += "<li class='node collapsed'><a href='#'>" + c + "</a><ul>";
            for (var s in FixedStars[c]) {
                list += "<li><a href='#S" + FixedStars[c][s].id + "'>" + FixedStars[c][s].name + "</a></li>";
            }
            list += "</ul></li>";
        }
    }

    list += "</ul>";
    $("#fixed-stars").append(list);
    $("#fixed-stars li li a").click(function () {
        var star_id = $(this).attr('href').substr(1);
        sky.AddStar({ name: $(this).text(), id: star_id }, 0);
        RefreshUI(sky);
    });
    //reinitTrees();
}

function ShowLots(sky: Sky) {
    var list = "<div class='listview-outlook' data-role='listview'>";

    for (var c in Lots) {
        list += "<a class='list' id='lot" + c + "'><div class='list-content'>"
            + "<span class='list-title'>" + Lots[c].name + "</span>"
            + "<span class='list-subtitle'>" + Lots[c].src + "</span>"
            + "<span class='list-remark'>" + "<span class='cls-sym'>Q</span>: "
            + Lots[c].dayFormula + " | <span class='cls-sym'>R</span>: "
            + Lots[c].nightFormula + "</span>" + "</span>" + "</div></a>";
    }

    list += "</div>";
    $("#lots").append(list);
    $("#lots a").click(function () {
        var lot = Lots[this.id.substr(3)];
        sky.AddLot(lot);
    });
}

function SetupUI(sky: Sky) {
    for (var s in Signs)
        $("#" + Signs[s].name).addClass("light-" + Planets[Signs[s].ruler]);

    var cols = [
        "yellow", "lightgrey", "purple", "lawngreen", "orangered",
        "blue", "black"];

    for (var i of PlntIterator)
        SetHighlights(i, cols[i], sky);
}

function PlntTag(tag, txt, plnt) {
    var cssclass = "";
    if (Array.isArray(plnt)) {
        for (var p in plnt)
            cssclass += "light-" + plnt[p] + " ";
    } else
        cssclass = "light-" + plnt;
    return "<" + tag + " class='" + cssclass + "'>" + txt + "</" + tag + ">";
}

function PlntTh(plnt) {
    return PlntTag("th", plnt, plnt);
}

function TablePositions(sky: Sky): string {
    function MakeLine(b) {
        var str = "<tr><th>" + b.name + "</th><td>" + ToDMSS(b.pos);
        if (b.speed != undefined) {
            str += ((b.speed < 0) ? " (Rx)" : "")
            + "</td><td>" + ToDMS(b.speed) + "</td>"
        } else {
            str += "</td><td>undefined</td>";
        }
        str += "</tr>";
        return str;
    }

    function MakeDeletable(bs) {
        var table = "";
        for (var b in bs)
            table += "<tr><th>" + bs[b].name + "</th><td>" + ToDMSS(bs[b].pos)
                + "</td><td data-lol='" + b + "'>X</td></tr>";
        return table;
    }

    var table = "";

    table += "<table class='striped table'>"
        + "<tr><th>Body</th><th>Position</th><th>Speed</th></tr>";
    ForeachPermanent(sky, (sky, body) => {
        table += MakeLine(body);
    });

    table += MakeDeletable(sky.stars);
    table += MakeDeletable(sky.lots);

    table += "</table>";

    return table;
}

function TableDignities(sky: Sky): string {
    var table = "";

    table += "<table class='striped table'><tr><th></th>";
    for (var p of PlntIterator)
        table += PlntTh(Planets[p]);

    table += "</tr><tr><th>Secte</th>";
    for (var p of PlntIterator)
        if (IsInSect(sky, p))
            table += PlntTag("td", "OUI", Planets[p]);
        else
            table += "<td>-</td>";

    table += "</tr><tr><th>Secte(signe)</th>";
    for (var p of PlntIterator)
        if (IsDiurn(sky, p) == (sky.planets[p].sign % 2 == 0))
            table += PlntTag("td", "OUI", Planets[p]);
        else
            table += "<td>-</td>";

    table += "</tr><tr><th>Domicile</th>";
    for (var p of PlntIterator)
        if (IsInSign(sky, p))
            table += PlntTag("td", "OUI", Planets[p]);
        else
            table += "<td>-</td>";

    table += "</th><tr><th>Exaltation</th>";
    for (var p of PlntIterator)
        if (IsExalted(sky, p))
            table += PlntTag("td", "OUI", Planets[p]);
        else
            table += "<td>-</td>";

    table += "</th><tr><th>Tripl.</th>";
    for (var p of PlntIterator)
        if (IsInTripl(sky, p))
            table += PlntTag("td", "OUI", Planets[p]);
        else
            table += "<td>-</td>";

    table += "</th><tr><th>Terme</th>";
    for (var p of PlntIterator) {
        var term = FindTerm(sky, sky.planets[p]);
        if (term == p)
            table += PlntTag("td", "OUI", Planets[p]);
        else
            table += PlntTag("td", Planets[term], Planets[term]);
    }

    table += "</th><tr><th>Décan</th>";
    for (var p of PlntIterator) {
        var face: Planets = FindFace(sky, p);
        if (face == p)
            table += PlntTag("td", "OUI", Planets[p]);
        else
            table += PlntTag("td", Planets[face], Planets[face]);
    }

    table += "</th><tr><th>Chute</th>";
    for (var p of PlntIterator)
        if (p == Signs[(sky.planets[p].sign + 6) % 12].exaltation)
            table += PlntTag("td", "OUI", Planets[p]);
        else
            table += "<td>-</td>";

    table += "</tr></table>";

    table += "<table class='striped table'><tr><th>Planete</th><th>Terme</th>";
    ForeachBody(sky, (sky, body) => {
        var term = Planets[FindTerm(sky, body)];
        table += "<tr><th>" + body.name + "</th>" + PlntTag("td", term, term) + "</tr>";
    });
    table += "</table>";
    return table;
}

function TableAspects(sky: Sky): string {
    var table = "";

    table += "<table class='table striped'><tr><th></th>";
    for (var p in PlntIterator)
        table += PlntTh(Planets[p]);
    table += "</tr>";
    for (var p in PlntIterator) {
        table += "<tr>" + PlntTh(Planets[p]);
        for (var q in PlntIterator) {
            var asp = Aspect(sky, sky.planets[p], sky.planets[q]);
            if (asp && p != q)
                table += PlntTag("td", AspectToSym(asp), [Planets[p], Planets[q]]);
            else
                table += "<td>-</td>";
        }
        table += "</tr>";
    }
    table += "</table>";
    return table;
}

function DrawResume(sky: Sky) {
    $("#resume-table").empty();
    $("#positions").html('<h2>Positions</h2>' + TablePositions(sky));

    $("#joys").html("<h2>Dignités</h2>" + TableDignities(sky));

    $("#aspects").html("<h2>Aspects</h2>" + TableAspects(sky));
}

function DrawLine(x1, y1, x2, y2, color, sym) {
    if (y1 < y2) {
        var pom = y1;
        y1 = y2;
        y2 = pom;
        pom = x1;
        x1 = x2;
        x2 = pom;
    }

    var symdiv = "<div class='cls-sym' style='" + "left:" + ((x1 + x2) / 2) + "px;" + "top:" + ((y1 + y2) / 2) + "px;'>" + sym + "</div>";

    var a = Math.abs(x1 - x2);
    var b = Math.abs(y1 - y2);
    var c;
    var sx = (x1 + x2) / 2;
    var sy = (y1 + y2) / 2;
    var width = Math.sqrt(a * a + b * b);
    var x = sx - width / 2;
    var y = sy;

    a = width / 2;

    c = Math.abs(sx - x);

    b = Math.sqrt(Math.abs(x1 - x) * Math.abs(x1 - x) + Math.abs(y1 - y) * Math.abs(y1 - y));

    var cosb = (b * b - a * a - c * c) / (2 * a * c);
    var rad = Math.acos(cosb);
    var deg = (rad * 180) / Math.PI;

    var div = '<div style="' + 'border:1px solid ' + color + ';' + 'width:' + width + 'px;' + 'height:0px;' + '-moz-transform:rotate(' + deg + 'deg);' + '-webkit-transform:rotate(' + deg + 'deg);' + 'position:absolute;' + 'top:' + y + 'px;' + 'left:' + x + 'px;' + 'box-shadow: 0px 0px 10px ' + color + ';' + 'transition: opacity 0.5s ease-in-out;' + 'opacity:0.5;"></div>';

    $("#lines").append(div);
    $("#lines").append(symdiv);
}

function LinkPlanets(sky: Sky, p: Body, q: Body, asp: Aspects) {
    var pos1 = WhereIs(p.pos, sky.Asc.pos);
    var pos2 = WhereIs(q.pos, sky.Asc.pos);
    var col;
    var sym;

    switch (asp) {
        case Aspects.Conjunction:
            col = "purple";
            sym = "!";
            break;
        case Aspects.Sextile:
            col = "lawngreen";
            sym = "'";
            break;
        case Aspects.Square:
            col = "red";
            sym = "#";
            break;
        case Aspects.Trine:
            col = "blue";
            sym = "$";
            break;
        case Aspects.Opposition:
            col = "black";
            sym = "\"";
            break;
        default:
            col = "pink";
            break;
    }

    DrawLine(pos1.x, pos1.y, pos2.x, pos2.y, col, sym);
}

function ToDMSS(angle: number): string {
    var signs = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"];
    var pos_sign = Math.floor(angle / 30);
    angle = angle % 30;
    return ToDMS(angle) + "<span class='cls-sym'>" + signs[pos_sign] + "</span>";
}

function ToDMS(angle: number): string {
    var str = "";
    str += Math.floor(angle) + "°";
    var min = (angle - Math.floor(angle)) * 60.0;
    str += Math.floor(min) + "'";
    min = (min - Math.floor(min)) * 60.0;
    str += Math.floor(min) + "\"";

    return str;
}
