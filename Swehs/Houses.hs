{-# LANGUAGE TemplateHaskell #-}
module Swehs.Houses where

import Data.Aeson.TH
import Data.Char (toLower)

data AscMc = AscMc { asc :: Double
                   , mc :: Double
                   , armc :: Double
                   , vertex :: Double
                   , equatorialAsc :: Double
                   , coAscKoch :: Double
                   , coAscMunkasey :: Double
                   , poAscMunkasey :: Double }
                   deriving (Show)

$(deriveToJSON  defaultOptions{constructorTagModifier = map toLower} ''AscMc)

