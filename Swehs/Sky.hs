{-# LANGUAGE OverloadedStrings #-}
module Swehs.Sky where

import Swehs.Date
import Swehs.Geo
import Swehs.Houses
import Swehs.Ipl
import Swehs.Sweph
import Data.Aeson
import qualified Data.Text as T (pack)
import qualified Data.ByteString.Lazy as BS (putStrLn)

planets :: [IplFlag]
planets = [Moon, Mercury, Venus, Sun, Mars, Jupiter, Saturn, TrueNode]

data Sky = Sky [(IplFlag, Pos)] AscMc

instance ToJSON Sky where
    toJSON (Sky plnts ascmc) =
        object [ "sky" .=
            (object $ map pairJSON plnts
                ++ [ "Asc" .= asc ascmc, "MC" .= mc ascmc ]) ]
        where
            pairJSON (k, v) = T.pack (show k) .= toJSON v

initSwe :: IO ()
initSwe = do
    setEphePath "ephe"
    setSidMode Lahiri

for = flip map

computeSky :: Gregorian -> Geopos -> Sky
computeSky date pos =
    let jd = julday date
        plnts = for planets $ \p ->
            (p, calcUt jd [Sidereal, Speed3] p)
        (houses, _) = housesEx jd [Sidereal] pos
    in
        Sky plnts houses
