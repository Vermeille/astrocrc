{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
module Swehs.Geo where

import Data.Int
import Swehs.Date
import Data.Aeson
import Database.HDBC
import Data.Bits (shiftL)
import Data.Convertible.Base
import Control.Applicative ((<$>), (<*>))
import Text.RawString.QQ

data CityInfo = CityInfo    { cityId :: Int
                            , cityName :: String
                            , cityValue :: String
                            , cityTZ :: String }
                            deriving (Show)

instance Convertible [SqlValue] CityInfo where
    safeConvert [cid, cname, ccountry, cadmin1, cTZ] =
        CityInfo <$> safeConvert cid
                <*> safeConvert cname
                <*> cvalue
                <*> safeConvert cadmin1
        where
            cvalue = mkVal <$> safeConvert cname <*> safeConvert ccountry
            mkVal c code = c ++ " (" ++ code ++ ")"
    safeConvert _ = Left $ ConvertError "" "" "" "more or less than 4 elm"

data Geopos = Geopos Double Double String deriving (Show)

instance Convertible [SqlValue] Geopos where
    safeConvert [lat, lon, tz] =
        Geopos <$> safeConvert lat <*> safeConvert lon <*> safeConvert tz
    safeConvert _ = Left $ ConvertError "" "" "" "more or less than 3 elm"

instance ToJSON CityInfo where
    toJSON ci = object [ "id" .= cityId ci
                        , "name" .= cityName ci
                        , "timezone".= cityTZ ci
                        , "value" .= cityValue ci ]

findCityInfo :: IConnection conn => conn -> String -> IO [CityInfo]
findCityInfo conn city = quickQuery' conn
    [r| SELECT id,name,country,timezone,admin1
        FROM geonames
        WHERE (name LIKE ? OR asciiname LIKE ?) LIMIT 10 |]
        [toSql cityPat, toSql cityPat] >>= return . map convert
    where
        cityPat = city ++ "%"

findGeopos :: IConnection conn => conn -> Int -> IO Geopos
findGeopos conn cityId = quickQuery' conn
    [r| SELECT latitude,longitude,timezone FROM geonames WHERE id=? |]
    [toSql cityId] >>= return . convert . head

findOffset :: IConnection conn => conn -> Gregorian -> String -> IO Int
findOffset conn date zone = quickQuery' conn
    [r| SELECT p
        FROM data
            INNER JOIN tz ON z = (CASE WHEN link = 0 THEN id ELSE link END)
        WHERE (o < ? AND name = ?)
        ORDER BY o DESC LIMIT 1 |]
        [toSql $ dateAsNum date, toSql zone] >>= return . convert . head . head
        where
            dateAsNum :: Gregorian -> Int64
            dateAsNum (Gregorian y m d t _) =
                let h = truncate t
                    dec :: Double -> Double
                    dec x = x - (fromIntegral $ truncate x)
                    min = truncate $ dec t * 60
                    s = truncate $ (dec t * 60) * 60
                in
                    ((fromIntegral y * 512 + fromIntegral m * 32 + fromIntegral d)
                        `shiftL` 17) + (h * 3600) + (min * 60) + s


