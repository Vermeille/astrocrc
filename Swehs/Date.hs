module Swehs.Date where

data TZ = TZ Int deriving (Show)
data JulianDay = JulianDay Double TZ deriving (Show)
data Gregorian = Gregorian Int Int Int Double TZ deriving (Show)


