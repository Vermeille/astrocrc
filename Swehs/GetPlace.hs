{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import Geo
import Data.Aeson
import Database.HDBC
import System.Environment
import Database.HDBC.Sqlite3
import Data.Convertible.Base
import qualified Data.ByteString.Lazy as BS (putStrLn)

main = do
    args <- getArgs
    let city = args !! 0
    db <- connectSqlite3 "geonames.sql"
    res <- findCityInfo db city
    BS.putStrLn . encode $ res
    disconnect db

