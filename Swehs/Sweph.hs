{-# LANGUAGE ForeignFunctionInterface #-}
{-# LANGUAGE OverloadedStrings #-}
module Swehs.Sweph where

import Swehs.Date
import Swehs.Geo
import Swehs.Ipl
import Foreign.C
import Data.Aeson
import Foreign.Ptr
import Swehs.Houses
import Data.Char (ord)
import Data.Bits ((.|.))
import Foreign.Storable
import System.IO.Unsafe (unsafePerformIO)
import Control.Applicative ((<$>), (<*>), pure)
import Foreign.Marshal.Alloc (alloca)
import Foreign.Marshal.Array (allocaArray, peekArray)

data Pos = Pos { longitude :: Double
                , latitude :: Double
                , distance :: Double
                , longSpeed :: Double
                , latSpeed :: Double
                , distSpeed :: Double }
                deriving (Show)

instance ToJSON Pos where
    toJSON p = object [ "pos" .= longitude p
                      , "speed" .= longSpeed p ]

foreign import ccall unsafe "swedate.h swe_revjul" c_revjul
    :: CDouble -> CInt -> Ptr CInt -> Ptr CInt -> Ptr CInt -> Ptr CDouble -> IO CInt

revjul :: JulianDay -> Gregorian
revjul (JulianDay jd _) = unsafePerformIO $
        alloca $ \yptr ->
            alloca $ \mptr ->
                alloca $ \dptr ->
                    alloca $ \tptr -> do
                        _ <- c_revjul (realToFrac jd) 1 yptr mptr dptr tptr
                        y <- fromIntegral <$> peek yptr
                        m <- fromIntegral <$> peek mptr
                        d <- fromIntegral <$> peek dptr
                        t <- realToFrac <$> peek tptr
                        return $ Gregorian y m d t (TZ 0)

foreign import ccall unsafe "swephexp.h swe_set_ephe_path" c_setEphePath
    :: CString -> IO ()

setEphePath :: String -> IO ()
setEphePath p = withCString p c_setEphePath

foreign import ccall unsafe "swedate.h swe_julday" c_julday
    :: CInt -> CInt -> CInt -> CDouble -> CInt -> IO CDouble

julday :: Gregorian -> JulianDay
julday (Gregorian y m d t tz) = unsafePerformIO $
        (JulianDay . realToFrac) <$> c_julday
                                    (fromIntegral y)
                                    (fromIntegral m)
                                    (fromIntegral d)
                                    (realToFrac t)
                                    1
                        <*> pure tz

foreign import ccall unsafe "swephexp.h swe_set_sid_mode" c_sidMode
    :: CInt -> CDouble -> CDouble -> IO ()

setSidMode :: SidMode -> IO ()
setSidMode sid@(SidUser t0 ayan_t0) =
        c_sidMode (fromIntegral $ fromEnum sid)
                    (realToFrac t0)
                    (realToFrac ayan_t0)
setSidMode sid = c_sidMode (fromIntegral $ fromEnum sid) 0 0

foreign import ccall unsafe "swephexp.h swe_calc_ut" c_calcUt
    :: CDouble -> CInt -> CInt -> Ptr CDouble -> CString -> IO Int

calcUt :: JulianDay -> [IFlag] -> IplFlag -> Pos
calcUt (JulianDay tjd _) flag ipl = unsafePerformIO $
        allocaArray 6 $ \xx ->
            allocaArray 256 $ \err -> do
                res <- c_calcUt (realToFrac tjd)
                                (fromIntegral $ fromEnum ipl)
                                (fromIntegral $ toFlag flag)
                                (xx :: Ptr CDouble)
                                (err :: CString)
                Pos <$> (realToFrac <$> peekElemOff xx 0)
                        <*> (realToFrac <$> peekElemOff xx 1)
                        <*> (realToFrac <$> peekElemOff xx 2)
                        <*> (realToFrac <$> peekElemOff xx 3)
                        <*> (realToFrac <$> peekElemOff xx 4)
                        <*> (realToFrac <$> peekElemOff xx 5)

foreign import ccall unsafe "swephexp.h swe_fixstar_ut" c_fixstar
    :: CString -> CDouble -> CInt -> Ptr CDouble -> CString -> IO CLong

fixstar :: String -> JulianDay -> [IFlag] -> Pos
fixstar star (JulianDay tjd _) flag = unsafePerformIO $
    withCString star $ \cstr -> do
        allocaArray 6 $ \xx ->
            allocaArray 256 $ \err -> do
                res <- c_fixstar cstr
                                (realToFrac tjd)
                                (fromIntegral $ toFlag flag)
                                (xx :: Ptr CDouble)
                                (err :: CString)
                Pos <$> (realToFrac <$> peekElemOff xx 0)
                        <*> (realToFrac <$> peekElemOff xx 1)
                        <*> (realToFrac <$> peekElemOff xx 2)
                        <*> (realToFrac <$> peekElemOff xx 3)
                        <*> (realToFrac <$> peekElemOff xx 4)
                        <*> (realToFrac <$> peekElemOff xx 5)

foreign import ccall unsafe "swephexp.h swe_houses_ex" c_housesEx
    :: CDouble -> CInt -> CDouble -> CDouble -> CInt -> Ptr CDouble -> Ptr CDouble -> IO Int

housesEx :: JulianDay -> [IFlag] -> Geopos -> (AscMc, [Double])
housesEx (JulianDay tjd _) iflag (Geopos lat lon _) = unsafePerformIO $
    allocaArray 13 $ \cusps ->
        allocaArray 10 $ \ascmc -> do
            res <- c_housesEx (realToFrac tjd)
                        (fromIntegral $ toFlag iflag)
                        (realToFrac lat)
                        (realToFrac lon)
                        (fromIntegral $ ord 'W')
                        cusps
                        ascmc
            a <- AscMc <$> (realToFrac <$> peekElemOff ascmc 0)
                        <*> (realToFrac <$> peekElemOff ascmc 1)
                        <*> (realToFrac <$> peekElemOff ascmc 2)
                        <*> (realToFrac <$> peekElemOff ascmc 3)
                        <*> (realToFrac <$> peekElemOff ascmc 4)
                        <*> (realToFrac <$> peekElemOff ascmc 5)
                        <*> (realToFrac <$> peekElemOff ascmc 6)
                        <*> (realToFrac <$> peekElemOff ascmc 7)
            b <- map realToFrac <$> peekArray 13 cusps
            return (a, b)

toFlag :: Enum a => [a] -> Int
toFlag = foldr (\f res -> fromEnum f .|. res) 0

toUT :: Gregorian -> Gregorian
toUT = revjul . addOffset . julday
    where
        addOffset (JulianDay d (TZ off)) = JulianDay (d - fromIntegral off / 86400) (TZ 0)
