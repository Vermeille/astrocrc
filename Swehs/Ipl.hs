{-# LANGUAGE CPP #-}
{-# LANGUAGE TemplateHaskell #-}
module Swehs.Ipl where

import Data.Aeson.TH

#define SE_NPLANETS    23
#define SE_AST_OFFSET  10000
#define SE_VARUNA  (SE_AST_OFFSET + 20000)

#define SE_FICT_OFFSET          40
#define SE_FICT_OFFSET_1        39
#define SE_FICT_MAX            999
#define SE_NFICT_ELEM           15
#define SE_COMET_OFFSET       1000
#define SE_NALL_NAT_POINTS      (SE_NPLANETS + SE_NFICT_ELEM))

data IplFlag =
        IplFlagEmpty
#define X(Flag, N) | Flag
# include "Ipl.def"
#undef X
        deriving (Show, Ord, Eq)

$(deriveToJSON  defaultOptions ''IplFlag)

instance Enum IplFlag where
#define X(Flag, N) fromEnum Flag = N
# include "Ipl.def"
#undef X

#define X(Flag, N) toEnum (N) = Flag
# include "Ipl.def"
#undef X
    toEnum _ = undefined

data IFlag =
        IFlagEmpty
#define X(Flag, N) | Flag
# include "flag.def"
#undef X
        deriving (Show)

instance Enum IFlag where
#define X(Flag, N) fromEnum Flag = N
# include "flag.def"
#undef X

#define X(Flag, N) toEnum (N) = Flag
# include "flag.def"
#undef X
    toEnum _ = undefined

data SidMode =
        SidFlagEmpty
#define X(Flag, N) | Flag
# include "sidereal.def"
#undef X
    | SidUser Double Double
    deriving (Show)

instance Enum SidMode where
#define X(Flag, N) fromEnum Flag = N
# include "sidereal.def"
#undef X
    fromEnum (SidUser _ _) = 255

#define X(Flag, N) toEnum (N) = Flag
# include "sidereal.def"
#undef X
    toEnum 255 = (SidUser 0 0)
    toEnum _ = undefined

