///<reference path="jquery.d.ts"/>

class Point {
    x: number;
    y: number;

    constructor(x_: number, y_: number) {
        this.x = x_;
        this.y = y_;
    }
}

function WhereIs(angle: number, asc: number): Point {
    angle =  angle - Math.floor(asc / 30) * 30 + 390;
    angle %= 360;

    var mod = Math.floor((Math.floor(angle) % 90) / 30);
    if (Math.floor(angle / 90) == 0) {
        switch (mod) {
            case 0:
                return new Point(15, 40 + ((angle % 30) / 30.0) * 160);
            case 1:
                return new Point(100, 160 + ((angle % 30) / 30.0) * 160);
            case 2:
                return new Point(15, 280 + ((angle % 30) / 30.0) * 160);
        }
    } else if (Math.floor(angle / 90) == 1) {
        switch (mod) {
            case 0:
                return new Point(40 + ((angle % 30)/30.0)*160, 460);
            case 1:
                return new Point(160 + ((angle % 30)/30.0)*160, 380);
            case 2:
                return new Point(280 + ((angle % 30)/30.0)*160, 460);
        }
    } else if (Math.floor(angle / 90) == 2) {
        switch (mod) {
            case 0:
                return new Point(460, 440 - ((angle % 30)/30.0)*160);
            case 1:
                return new Point(380, 320 - ((angle % 30)/30.0)*160);
            case 2:
                return new Point(460, 200 - ((angle % 30)/30.0)*160);
        }
    } else {
        switch (mod) {
            case 0:
                return new Point(440 - ((angle % 30)/30.0)*160, 20);
            case 1:
                return new Point(320 - ((angle % 30)/30.0)*160, 90);
            case 2:
                return new Point(200 - ((angle % 30)/30.0)*160, 20);
        }
    }
}

function PlaceOneSymbol(asc: Body, b: Body, id: string): void {
    var pos = WhereIs(b.pos, asc.pos);
    $("#" + id).animate({left: pos.x, top: pos.y});
}

function getParams() {
    var query = window.location.search.substring(1).replace(/\+/g, " ");
    var vars = query.split('&');

    var res = {};
    for (var eq in vars) {
        var val = vars[eq].split('=');
        res[val[0]] = decodeURIComponent(val[1]);
    }
    return res;
}

enum Aspects { Conjunction, Opposition, Square, Trine, Sextile }

enum Planets { Sun, Moon, Mercury, Venus, Mars, Jupiter, Saturn }

var PlntIterator = [
    Planets.Sun, Planets.Moon, Planets.Mercury, Planets.Venus, Planets.Mars,
    Planets.Jupiter, Planets.Saturn
];

enum Elements { Fire, Earth, Air, Water }

var Signs = [
    {
        name: "Aries",
        ruler: Planets.Mars,
        exaltation: Planets.Sun,
        element: Elements.Fire
    },
    {
        name: "Taurus",
        ruler: Planets.Venus,
        exaltation: Planets.Moon,
        element: Elements.Earth
    },
    {
        name: "Gemini",
        ruler: Planets.Mercury,
        exaltation: null,
        element: Elements.Air
    },
    {
        name: "Cancer",
        ruler: Planets.Moon,
        exaltation: Planets.Jupiter,
        element: Elements.Water
    },
    {
        name: "Leo",
        ruler: Planets.Sun,
        exaltation: null,
        element: Elements.Fire
    },
    {
        name: "Virgo",
        ruler: Planets.Mercury,
        exaltation: Planets.Mercury,
        element: Elements.Earth
    },
    {
        name: "Libra",
        ruler: Planets.Venus,
        exaltation: Planets.Saturn,
        element: Elements.Air
    },
    {
        name: "Scorpio",
        ruler: Planets.Mars,
        exaltation: null,
        element: Elements.Water
    },
    {
        name: "Sagittarius",
        ruler: Planets.Jupiter,
        exaltation: null,
        element: Elements.Fire
    },
    {
        name: "Capricorn",
        ruler: Planets.Saturn,
        exaltation: Planets.Mars,
        element: Elements.Earth
    },
    {
        name: "Aquarius",
        ruler: Planets.Saturn,
        exaltation: null,
        element: Elements.Air
    },
    {
        name: "Pisces",
        ruler: Planets.Jupiter,
        exaltation: Planets.Venus,
        element: Elements.Water
    }
];

var Triplicities = {
    Fire:  [Planets.Sun,    Planets.Jupiter, Planets.Saturn],
    Earth: [Planets.Venus,  Planets.Moon,    Planets.Mars],
    Air:   [Planets.Saturn, Planets.Mercury, Planets.Jupiter],
    Water: [Planets.Venus,  Planets.Mars,    Planets.Moon]
};

var Terms = [
    // Aries
    {deg: 6, ruler: Planets.Jupiter}, {deg: 12, ruler: Planets.Venus},
    {deg: 20, ruler: Planets.Mercury}, {deg: 25, ruler: Planets.Mars},
    {deg: 30, ruler: Planets.Saturn},
    // Taurus
    {deg: 38, ruler: Planets.Venus}, {deg: 44, ruler: Planets.Mercury},
    {deg: 52, ruler: Planets.Jupiter}, {deg: 57, ruler: Planets.Saturn},
    {deg: 60, ruler: Planets.Mars},
    // Gemini
    {deg: 66, ruler: Planets.Mercury}, {deg: 72, ruler: Planets.Jupiter},
    {deg: 77, ruler: Planets.Venus}, {deg: 84, ruler: Planets.Mars},
    {deg: 90, ruler: Planets.Saturn},
    // Cancer
    {deg: 97, ruler: Planets.Mars}, {deg: 103, ruler: Planets.Venus},
    {deg: 109, ruler: Planets.Mercury}, {deg: 116, ruler: Planets.Jupiter},
    {deg: 120, ruler: Planets.Saturn},
    // Leo
    {deg: 126, ruler: Planets.Jupiter}, {deg: 131, ruler: Planets.Venus},
    {deg: 138, ruler: Planets.Saturn}, {deg: 144, ruler: Planets.Mercury},
    {deg: 150, ruler: Planets.Mars},
    // Virgo
    {deg: 157, ruler: Planets.Mercury}, {deg: 167, ruler: Planets.Venus},
    {deg: 171, ruler: Planets.Jupiter}, {deg: 178, ruler: Planets.Mars},
    {deg: 180, ruler: Planets.Saturn},
    // Libra
    {deg: 186, ruler: Planets.Saturn}, {deg: 191, ruler: Planets.Mercury},
    {deg: 199, ruler: Planets.Jupiter}, {deg: 206, ruler: Planets.Venus},
    {deg: 210, ruler: Planets.Mars},
    // Scorpio
    {deg: 217, ruler: Planets.Mars}, {deg: 221, ruler: Planets.Venus},
    {deg: 229, ruler: Planets.Mercury}, {deg: 234, ruler: Planets.Jupiter},
    {deg: 240, ruler: Planets.Saturn},
    // Sagittarius
    {deg: 252, ruler: Planets.Jupiter}, {deg: 257, ruler: Planets.Venus},
    {deg: 261, ruler: Planets.Mercury}, {deg: 266, ruler: Planets.Saturn},
    {deg: 270, ruler: Planets.Mars},
    // Capricorn
    {deg: 277, ruler: Planets.Mercury}, {deg: 284, ruler: Planets.Jupiter},
    {deg: 292, ruler: Planets.Venus}, {deg: 296, ruler: Planets.Saturn},
    {deg: 300, ruler: Planets.Mars},
    // Aquarius
    {deg: 307, ruler: Planets.Mercury}, {deg: 313, ruler: Planets.Venus},
    {deg: 320, ruler: Planets.Jupiter}, {deg: 325, ruler: Planets.Mars},
    {deg: 330, ruler: Planets.Saturn},
    // Pisces
    {deg: 342, ruler: Planets.Venus}, {deg: 346, ruler: Planets.Jupiter},
    {deg: 349, ruler: Planets.Mercury}, {deg: 358, ruler: Planets.Mars},
    {deg: 360, ruler: Planets.Saturn}
];

var Faces = {
    Aries:          [Planets.Mars,    Planets.Sun,     Planets.Venus],
    Taurus:         [Planets.Mercury, Planets.Moon,    Planets.Saturn],
    Gemini:         [Planets.Jupiter, Planets.Mars,    Planets.Sun],
    Cancer:         [Planets.Venus,   Planets.Mercury, Planets.Moon],
    Leo:            [Planets.Saturn,  Planets.Jupiter, Planets.Mars],
    Virgo:          [Planets.Sun,     Planets.Venus,   Planets.Mercury],
    Libra:          [Planets.Moon,    Planets.Saturn,  Planets.Jupiter],
    Scorpio:        [Planets.Mars,    Planets.Sun,     Planets.Venus],
    Sagittarius:    [Planets.Mercury, Planets.Moon,    Planets.Saturn],
    Capricorn:      [Planets.Jupiter, Planets.Mars,    Planets.Sun],
    Aquarius:       [Planets.Venus,   Planets.Mercury, Planets.Moon],
    Pisces:         [Planets.Saturn,  Planets.Jupiter, Planets.Mars]
};


function PlaceSigns(asc: number): void
{
    var asc_sign = Math.floor(asc / 30);
    for (var s in Signs)
    {
        var i = parseInt(s);
        $("#" + Signs[i].name).animate(
                $("#house" + ((i - asc_sign + 12) % 12 + 1)).css(["left", "top"]));
    }
}

function IsDiurnChart(sky: Sky): boolean {
    return IsAbove(sky, sky.planets[Planets.Sun]);
}

function IsAbove(sky: Sky, b: Body): boolean {
    switch (b.house) {
        case 1:
            return b.pos < sky.Asc.pos;
        case 2: case 3: case 3: case 5: case 6:
            return false;
        case 7:
            return b.pos > (sky.Asc.pos + 160) % 360;
        case 8: case 9: case 10: case 11: case 12:
            return true;
    }
}

function IsDiurn(bodies, planet: Planets): boolean {
    switch (planet) {
        case Planets.Sun:
        case Planets.Jupiter:
        case Planets.Saturn:
            return true;
        case Planets.Moon:
        case Planets.Mars:
        case Planets.Venus:
            return false;
        case Planets.Mercury:
            return IsAbove(bodies, bodies.planets[Planets.Sun]);
    }
}

function IsInSect(sky: Sky, p: Planets): boolean {
    var sun = sky.planets[Planets.Sun];
    var pl = sky.planets[p];
    if (IsDiurn(sky, p))
        return IsAbove(sky, pl) == IsAbove(sky, sun);
    else
        return IsAbove(sky, pl) != IsAbove(sky, sun);
}

function Aspect(bodies, p: Body, q: Body): Aspects {
    var diff = Math.max(p.sign, q.sign) - Math.min(p.sign, q.sign);
    switch (diff) {
        case 0:
            return Aspects.Conjunction;
        case 2: case 10:
            return Aspects.Sextile;
        case 3: case 9:
            return Aspects.Square;
        case 4: case 8:
            return Aspects.Trine;
        case 6:
            return Aspects.Opposition;
        default:
            return null;
    }
}

function MasterGeniture(sky: Sky): Planets {
    var next_sign = (sky.planets[Planets.Moon].sign + 1) % 12;

    if (next_sign == 3 || next_sign == 4)
        return Planets.Mercury;
    return Signs[next_sign].ruler;
}

function IsInTripl(sky: Sky, p: Planets): boolean {
    var element: Planets[] =
        Triplicities[Elements[Signs[sky.planets[p].sign].element]];
    return $.inArray(p, element) != -1;
}

function FindTerm(sky: Sky, b: Body): Planets {
    for (var t in Terms) {
        if (Terms[t].deg > b.pos)
            return Terms[t].ruler;
    }
}

function IsInTerm(sky: Sky, p: Planets): boolean {
    return FindTerm(sky, sky.planets[p]) == p;
}

function FindFace(sky: Sky, p: Planets): Planets {
    var in_sign = sky.planets[p].pos % 30;
    return Faces[Signs[sky.planets[p].sign].name][Math.floor(in_sign / 10)];
}

function IsExalted(sky: Sky, p: Planets): boolean {
    return p == Signs[sky.planets[p].sign].exaltation;
}

function IsInSign(sky: Sky, p: Planets): boolean {
    return p == Signs[sky.planets[p].sign].ruler;
}
