OUT ?= ./build
CXXFLAGS=-lsqlite3 -Wall -Wextra

all: backend frontend

####################### backend ##############################

backend: dist swe/libswe.a soft endpoints

dist:
	cabal sandbox init
	cabal install --only-dependencies

swe/libswe.a:
	make -C swe

endpoints:
	cp resources/backend/* $(OUT)
	cp eph -r $(OUT)

soft:
	cabal build
	mkdir $(OUT)
	cp dist/build/astro/astro $(OUT)

######################### frontend ###########################

frontend: ts js metro_rule

ts:
	tsc *.ts --out $(OUT)/out.js

js:
	cp resources/frontend/* $(OUT)

metro_rule:
	cp -r metro $(OUT)

clean:
	cd swehs && cabal clean
	make -C swe/ clean
