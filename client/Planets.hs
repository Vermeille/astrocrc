module Planets where

data Planet = Saturn | Jupiter | Mars | Sun | Venus | Mercury | Moon
        deriving (Show)

sect :: Planet -> Sect
sect Saturn     = Diurnal
sect Jupiter    = Diurnal
sect Mars       = Nocturnal
sect Sun        = Diurnal
sect Venus      = Nocturnal
sect Mercury    = error "Mercury has no sect on its own"
sect Moon       = Nocturnal

