module Zodiac where

data Sign   = Aries
            | Taurus
            | Gemini
            | Cancer
            | Leo
            | Virgo
            | Libra
            | Scorpio
            | Sagittarius
            | Capricorn
            | Aquarius
            | Pisces
            deriving (Show)

instance Enum Sign where
    fromEnum Aries      = 0
    fromEnum Taurus     = 1
    fromEnum Gemini     = 2
    fromEnum Cancer     = 3
    fromEnum Leo        = 4
    fromEnum Virgo      = 5
    fromEnum Libra      = 6
    fromEnum Scorpio    = 7
    fromEnum Sagittarius= 8
    fromEnum Capricorn  = 9
    fromEnum Aquarius   = 10
    fromEnum Pisces     = 11

    toEnum 0  = Aries
    toEnum 1  = Taurus
    toEnum 2  = Gemini
    toEnum 3  = Cancer
    toEnum 4  = Leo
    toEnum 5  = Virgo
    toEnum 6  = Libra
    toEnum 7  = Scorpio
    toEnum 8  = Sagittarius
    toEnum 9  = Capricorn
    toEnum 10 = Aquarius
    toEnum 11 = Pisces

data Sect = Diurnual | Nocturnal

domicile :: Sign    -> Planet
domicile Aries      = Mars
domicile Taurus     = Venus
domicile Gemini     = Mercury
domicile Cancer     = Moon
domicile Leo        = Sun
domicile Virgo      = Mercury
domicile Libra      = Venus
domicile Scorpio    = Mars
domicile Sagittarius= Jupiter
domicile Capricorn  = Saturn
domicile Aquarius   = Saturn
domicile Pisces     = Jupiter

exaltation :: Sign  -> Maybe Planet
exaltation Aries    = Just Sun
exaltation Taurus   = Just Moon
exaltation Virgo    = Just Mercury
exaltation Pisces   = Just Venus
exaltation Capricorn= Just Mars
exaltation Cancer   = Just Jupiter
exaltation Libra    = Just Saturn
exaltation _        = Nothing

fall :: Sign -> Maybe Planet
fall Libra      = Just Sun
fall Scorpio    = Just Moon
fall Pisces     = Just Mercury
fall Virgo      = Just Venus
fall Pisces     = Just Mars
fall Capricorn  = Just Jupiter
fall Aries      = Just Saturn
fall _          = Nothing

data Element = Fire | Earth | Air | Water

element :: Sign -> Element
element s =
        case fromEnum s `mod` 3 of
            0 -> Fire
            1 -> Earth
            2 -> Air
            3 -> Water

sectSign :: Sign -> Sect
sectSign s =
        case fromEnum s `mod` 2 of
            0 -> Diurnal
            1 -> Nocturnal

dorothean :: Element -> (Planet, Planet, Planet)
dorothean Fire  = (Sun, Jupiter, Saturn)
dorothean Earth = (Venus, Moon, Mars)
dorothean Air   = (Saturn, Mercury, Jupiter)
dorothean Water = (Venus, Mars, Moon)

