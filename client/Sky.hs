module Sky where

data Aspects = Conjonction | Sextile | Square | Trine | Opposition

data Point = Point
        { name      :: String
        , pos       :: Double
        , house     :: Int
        , sign      :: Sign
        }

data Sky = Sky
        { asc       :: Point
        , mc        :: Point
        , planets   :: [Point]
        , cusps     :: [Point]
        , stars     :: [Point]
        , lots      :: [Point]
        }

addStar :: Sky -> Point -> Sky
addStar s p = s { stars = p : (filter ((/= name p) . name) $ stars s) }

addLot :: Sky -> Point -> Sky
addLot s p = s { lots = p : (filter ((/= name p) . name) $ lots s) }

calcSign :: Double -> Sign
calcSign = toEnum . truncate . (/ 30)

